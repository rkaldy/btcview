#include <Eigen/Dense>

using namespace Eigen;

/**
 * Function and tests related to normal distribution
 */
class normal {
public:

	/*
	 * Density function
	 * (-∞,∞) -> (0,∞)
	 */
	static double dens(double x, double mu = 0, double sigma = 1);

	/*
	 * Cumulative distribution function 
	 * (-∞,∞) -> (0,1)
	 */
	static double dist(double x, double mu = 0, double sigma = 1);

	/*
	 * Quantile function (CDF inverse)
	 * (0,1) -> (-∞, ∞)
	 */
	static double quant(double p, double mu = 0, double sigma = 1);

	/*
	 * Shapiro-Wilk normality test
	 *
	 * @in x		Samples. This method will sort it.
	 *
	 * @return		p-value
	 */
	static double shapiro_wilk(VectorXd& x);
};

#ifndef MULTICYCLE_H
#define MULTICYCLE_H


#define MULTI_CYCLE_BEGIN(dim, init)		\
do {										\
	int di = -1;							\
	do {									\
		for (di++; di < (dim); di++) {		\
			init;							\
		}

#define MULTI_CYCLE_END(cond, inc)			\
		do {								\
			di--;							\
			inc;							\
		} while ((di >= 0) && !(cond));		\
	} while (di >= 0);						\
} while (0)


#define MULTI_CYCLE_BEGIN_EX(dim_from, dim_to, init)	\
do {										\
	int di = dim_from - 1;					\
	do {									\
		for (di++; di < (dim_to); di++) {	\
			init;							\
		}

#define MULTI_CYCLE_END_EX(dim_from, dim_to, cond, inc)	\
		do {											\
			di--;										\
			inc;										\
		} while ((di >= dim_from) && !(cond));			\
	} while (di >= dim_from);							\
} while (0)


#endif  // MULTICYCLE_H

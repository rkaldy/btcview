#include <cmath>
#include <cassert>
#include "moving_average.h"


moving_average::moving_average(int len, int off) :
	savitzky_golay(1, len, off)
{}


moving_average::moving_average(double len_eff, double off_ratio) :
	savitzky_golay(1, len_eff, off_ratio)
{}


VectorXd moving_average::smooth_simple(const VectorXd& src, int len, int off) {
	assert(len > 0);
	assert(off >= 0 && off < len);

	VectorXd sm(src.size());
	double s = src.head(len-1).sum();
	sm.head(off).setConstant(NAN);
	for (int x = off; x <= src.size()-len+off; x++) {
		s += src[x + len - off - 1];
		sm[x] = s / len;
		s -= src[x - off];
	}
	sm.tail(len-off-1).setConstant(NAN);
	return sm;
}

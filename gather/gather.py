#!/usr/bin/python3

import sys, MySQLdb
from datetime import datetime
from optparse import OptionParser
from enum import Enum
from local import *


TIMEZONE = " +0000"
TIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%d.%m.%Y"


class LogType(Enum):
    SIMPLE = 1
    BEGIN = 2
    CONTINUE = 3
    END = 4


def log(msg, type = LogType.SIMPLE):
    if (type == LogType.SIMPLE or type == LogType.BEGIN):
        sys.stdout.write(datetime.now().strftime("[%Y-%m-%d %H:%M:%S] "))
    sys.stdout.write(msg)
    if (type == LogType.SIMPLE or type == LogType.END):
        sys.stdout.write("\n")
    sys.stdout.flush()


def formatTime(timeInt):
    return datetime.utcfromtimestamp(timeInt).strftime(TIME_FORMAT)

def parseTime(timeStr, fmt = TIME_FORMAT):
    return datetime.strptime(timeStr + TIMEZONE, fmt + " %z").timestamp()

def now():
    return datetime.now().timestamp()


class DB:
    def __init__(self):
        global DB_DATABASE, DB_USER, DB_PASSWORD
        self.conn = MySQLdb.connect(user=DB_USER, passwd=DB_PASSWORD, db=DB_DATABASE)
        self.cursor().execute("SET time_zone = 'UTC'")

    def cursor(self):
        return MySQLdb.cursors.SSCursor(self.conn)
    
    def dropIndices(self):
        log("Dropping indices")
        cur = self.cursor()
        cur.execute("ALTER TABLE trans DISABLE KEYS")
#        cur.execute("ALTER TABLE depth DISABLE KEYS")
#        cur.execute("DROP INDEX IF EXISTS series_idx ON trans")
#        cur.execute("DROP INDEX IF EXISTS series_idx ON depth")

    def recreateIndices(self):
        log("Re-creating indices")
        cur = self.cursor()
        cur.execute("ALTER TABLE trans ENABLE KEYS")
        cur.execute("OPTIMIZE TABLE trans")
#        cur.execute("ALTER TABLE depth ENABLE KEYS")
#        cur.execute("OPTIMIZE TABLE depth")
#        cur.execute("CREATE INDEX series_idx ON trans (exchange_id, pair_id, `time`)")
#        cur.execute("CREATE INDEX series_idx ON depth (exchange_id, pair_id, `time`)")

    def query(self, sql, args, multi=False):
        if not type(args) is tuple:
            args = (args,)
        cur = self.cursor()
        cur.execute(sql, args)
        if multi:
            return cur.fetchall()
        else:
            return cur.fetchone()

    def insert(self, sql, args, multi=False):
        if not multi and not type(args) is tuple:
            args = (args,)
        cur = self.cursor()
        if multi:
            cur.executemany(sql, args)
            return cur.rowcount
        else:
            cur.execute(sql, args)
            return cur.lastrowid

    def getExchange(self, name):
        row = self.query("SELECT exchange_id FROM exchange WHERE name = %s", name)
        if row:
            return row[0]
        else:
            return None

    def getPair(self, code, createIfNotExist = False):
        row = self.query("SELECT pair_id FROM pair WHERE code = %s", code)
        if row:
            return row[0] 
        elif createIfNotExist:
            log("Inserting new pair %s" % code)
            return self.insert("INSERT INTO pair (code) VALUES (%s)", code)
        else:
            return None



class Gather:
    
    def __init__(self, exchange):
        self.db = DB()
        self.exchangeId = self.db.getExchange(exchange)
        if not self.exchangeId:
            raise Exception("Exchange not defined...yet")

    
    def getMinMaxTx(self, pairId):
        return self.db.query("SELECT MAX(trans_id), MIN(trans_id), UNIX_TIMESTAMP(MAX(time)), UNIX_TIMESTAMP(MIN(time)) FROM trans WHERE exchange_id = %s AND pair_id = %s", (self.exchangeId, pairId))

    def getTxCount(self, pairId):
        return self.db.query("SELECT COUNT(*) FROM trans WHERE exchange_id = %s AND pair_id = %s", (self.exchangeId, pairId))[0]

    
    def listCurrencies(self):
        pass
    
    def checkCurrency(self):
        pass

    def transactionsInInterval(self, pairId, code, start, end = None, idTo = None):
        pass

    
    def transactions(self, code, start, options):
        idxDropped = False
        try:
            self.checkCurrency(code)
            pairId = self.db.getPair(code, True)
            minmax = self.getMinMaxTx(pairId)

            if options.rewrite:
                self.db.query("DELETE FROM trans WHERE exchange_id = %s AND pair_id = %s", (self.exchangeId, pairId))
            
            if options.dropIdx:
                self.db.dropIndices()
                idxDropped = True
            if minmax[0] and not options.rewrite:
                self.transactionsInInterval(pairId, code, max(start, minmax[2]))
                self.transactionsInInterval(pairId, code, start, minmax[3], minmax[1])
            else:
                self.transactionsInInterval(pairId, code, start)
            log("%s contains %i transactions now" % (code, self.getTxCount(pairId)))

        except KeyboardInterrupt:
            log(" Interrupted", LogType.END)
            log("%s contains %i transactions now" % (code, self.getTxCount(pairId)))
        except Exception as e:
            log(" ERROR: " + str(e), LogType.END)
        
        if idxDropped:
            self.db.recreateIndices()

    
    def run(self):
        parser = OptionParser(usage="Usage: %prog [opts] pair-code date-from")
        parser.add_option("-l", dest="list", action="store_true", help="list all available currency pairs, do not gather anything")
        parser.add_option("-r", dest="rewrite", action="store_true", help="gather all txs from a given time, possibly overwrite old data in db")
        parser.add_option("-x", dest="dropIdx", action="store_false", default=True, help="do not temporarily drop db indices to speed up bulk inserting")

        (options, args) = parser.parse_args()
        if options.list:
            self.listCurrencies()
        else:
            if len(args) != 2:
                parser.print_help()
                sys.exit(1)
            self.transactions(args[0], parseTime(args[1], DATE_FORMAT), options)

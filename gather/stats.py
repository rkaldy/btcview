#!/usr/bin/python3

from gather import *

db = DB()
print("Exchange\tPair\t\tTX count\tFrom\t\t\tTo")
print("-" * 91)
for exchange in db.query("SELECT exchange_id, name FROM exchange ORDER BY exchange_id", (), multi=True):
    for pair in db.query("SELECT pair_id, code FROM pair ORDER BY pair_id", (), multi=True):
        stat = db.query("SELECT COUNT(*), MIN(time), MAX(time) FROM trans WHERE exchange_id = %s AND pair_id =%s", (exchange[0], pair[0]))
        if stat[0] != 0:
            print("%-10s\t%-10s\t%9i\t%s\t%s" % (exchange[1], pair[1], stat[0], stat[1], stat[2]))

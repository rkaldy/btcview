#!/usr/bin/python3

from gather import *

QUERY_INTERVAL = 10
DEPTH_LIMIT_COEF = 0.2


db = connectDb()
pairs = {}

while True:
    refreshPairs(db, pairs)
    time.sleep(QUERY_INTERVAL - now() % QUERY_INTERVAL)
    tick = (now() + QUERY_INTERVAL/2) // QUERY_INTERVAL * QUERY_INTERVAL
    timeFormatted = time.strftime(TIME_FORMAT, time.gmtime(tick))
    for code, pair in pairs.items():
        log("Gathering %s" % code, LogType.BEGIN)
        depth = req("getorderbook?market=%s&type=both" % code)
        if depth == None:
            log(" - data not available, removing", LogType.END)
            del pairs[code]
            continue

        price = req("getticker?market=" + code)["Last"]
        rowsInserted = 0
        for type in ['buy', 'sell']:
            for order in depth[type]:
                if order["Rate"] > price * (1 - DEPTH_LIMIT_COEF) and order["Rate"] < price * (1 + DEPTH_LIMIT_COEF):
                    db.execute("INSERT INTO depth (pair_id, time, type, price, vol) VALUES (%i, '%s', '%s', %.8f, %.8f)" % (pair.id, timeFormatted, type, order["Rate"], order["Quantity"]))
                    rowsInserted += 1
        log(" - %i orders inserted" % rowsInserted, LogType.END)

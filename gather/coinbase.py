#!/usr/bin/python3

import sys, requests, simplejson, time
from datetime import datetime
from gather import *

URL_BASE = "https://api.pro.coinbase.com/products/"
URL_BASE = "http://antonio.cz/"
TIME_FORMAT_JSON = "%Y-%m-%dT%H:%M:%S"


class CoinbaseGather(Gather):
    
    def __init__(self):
        Gather.__init__(self, "Coinbase")

    def req(self, url):
#        print(URL_BASE+url)
        data = requests.get(URL_BASE + url, timeout=5).json()
        if "message" in data:
            raise Exception(data["message"])
        else:
            return data

    def listCurrencies(self):
        try :
            data = self.req("")
            for curr in data:
                print(curr["id"])
        except Exception as e:
            print("ERROR: " + str(e))

    def checkCurrency(self, code):
        self.req(code)
        pass


    def transactionsInInterval(self, pairId, code, start, end = None, idTo = None):
        if idTo:
            after = idTo
        else:
            after = 99999999
        if not end:
            end = now()

        while start <= end:
            tries = 0
            while True:
                log("Retrieving trade history till %s..." % formatTime(end), LogType.BEGIN)
                try:
                    trans = self.req("%s/trades?after=%i" % (code, after))
                    log("%i txs retrieved" % len(trans), LogType.END)
                    break
                except requests.exceptions.Timeout:
                    log("Timeout expired", LogType.END)
                except simplejson.scanner.JSONDecodeError:
                    log("JSON parsing failed", LogType.END)

                tries += 1
                if tries == 3:
                    raise Exception("Request failed 3 times, will not try more")
                time.sleep(5)

            sql = "INSERT INTO trans (exchange_id, pair_id, trans_id, time, dir, price, vol) VALUES (%s, %s, %s, %s, %s, %s, %s) ON DUPLICATE KEY UPDATE dir = dir"
            data = []
            for tx in trans:
                # coinbase returns buy/sell from the maker's view
                if tx["side"] == "buy":
                    txdir = "sell"
                else:
                    txdir = "buy"
                data.append((self.exchangeId, pairId, tx["trade_id"], tx["time"], txdir, float(tx["price"]), float(tx["size"])))
            rowCount = self.db.insert(sql, data, multi=True)
            log("%i txs inserted" % rowCount)
            end = parseTime(trans[-1]["time"][:19], TIME_FORMAT_JSON)
            after = int(trans[-1]["trade_id"])
            time.sleep(COINBASE_REQ_SLEEP)


gather = CoinbaseGather()
gather.run()

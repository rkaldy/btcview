DROP DATABASE IF EXISTS `btc2`;
CREATE DATABASE `btc2`;
USE `btc2`;


CREATE TABLE `exchange` (
  `exchange_id` tinyint NOT NULL,
  `name` varchar(15) NOT NULL,
  PRIMARY KEY (`exchange_id`)
) ENGINE=MyISAM CHARSET=latin1;

INSERT INTO `exchange` VALUES (1, 'Bittrex');
INSERT INTO `exchange` VALUES (2, 'Poloniex');
INSERT INTO `exchange` VALUES (3, 'Coinbase');
INSERT INTO `exchange` VALUES (99, 'test');


CREATE TABLE `pair` (
  `pair_id` smallint NOT NULL AUTO_INCREMENT,
  `code` varchar(15) NOT NULL,
  PRIMARY KEY (`pair_id`)
) ENGINE=MyISAM CHARSET=latin1;


CREATE TABLE `depth` (
  `exchange_id` tinyint NOT NULL, 
  `pair_id` smallint NOT NULL,
  `time` datetime NOT NULL,
  `type` enum('buy','sell') NOT NULL,
  `price` double NOT NULL,
  `vol` double NOT NULL,
  PRIMARY KEY (`exchange_id`,`pair_id`,`time`,`type`,`price`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE INDEX `series_idx` ON `depth` (`exchange_id`,`pair_id`,`time`);


CREATE TABLE `trans` (
  `exchange_id` tinyint NOT NULL, 
  `pair_id` smallint NOT NULL,
  `trans_id` int NOT NULL,
  `time` datetime NOT NULL,
  `dir` enum('buy','sell') NOT NULL,
  `price` double NOT NULL,
  `vol` double NOT NULL,
  PRIMARY KEY (`exchange_id`,`pair_id`,`trans_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE INDEX `series_idx` ON `trans` (`exchange_id`,`pair_id`,`time`);

#!/usr/bin/python3

import sys, time
from gather import *
from bittrex import *

MIN_QUERY_INTERVAL = 6
MAX_QUERY_INTERVAL = 300
ADAPTIVE_INTERVAL_FRACTION = 7
EXCHANGE_ID = 1


db = db()
pairs = {}

while True:
    refreshPairs(db, pairs)
    startTime = now()
    for code, pair in pairs.items():
        try:
            if (now() < pair.lastQueryTime + pair.predInterval): continue
            log("Gathering %s" % code, LogType.BEGIN)
            trans = req("getmarkethistory?market=" + code)
            if not trans:
                log(" Data not available", LogType.END)
                continue
    
            pair.lastQueryTime = now()
            firstTime = parseBittrexTime(trans[len(trans) // ADAPTIVE_INTERVAL_FRACTION]["TimeStamp"])
            pair.predInterval = min(pair.lastQueryTime - firstTime, MAX_QUERY_INTERVAL)
    
            maxid = db.query("SELECT IFNULL(MAX(trans_id), 0) FROM trans WHERE pair_id = %s", pair.id)[0]
            sql = "INSERT INTO trans (exchange_id, pair_id, trans_id, time, dir, price, vol) VALUES (%s, %s, %s, %s, %s, %s, %s)" 
            data = []
            for tx in trans:
                if tx["Id"] > maxid:
                    data.append((EXCHANGE_ID, pair.id, tx["Id"], tx["TimeStamp"], tx["OrderType"], tx["Price"], tx["Quantity"]))
            rowCount = db.insert(sql, data, multi=True)
            log(" - %i txs inserted" % rowCount, LogType.END)
        except KeyboardInterrupt:
            log(" Interrupted", LogType.END)
            sys.exit(1)
        except Exception as e:
            log(" " + str(e), LogType.END)
    
    time.sleep(max(0, MIN_QUERY_INTERVAL - (now() - startTime)))

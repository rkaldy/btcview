#!/usr/bin/python3

import sys, requests, json
import MySQLdb as my
from datetime import datetime
from gather import *

BASE_URL = "https://poloniex.com/public?"
MONTH = 86400 * 30


class PoloniexGather(Gather):

    def __init__(self):
        Gather.__init__(self, "Poloniex")

    def req(self, url):
        data = requests.get(BASE_URL + url).json()
        if "error" in data:
            raise Exception(data["error"])
        else:
            return data


    def listCurrencies(self):
        data = self.req("command=return24hVolume")
        for curr in sorted(data.keys()):
            if curr[:5] != "total":
                print(curr.replace("_", "-"))


    def checkCurrency(self, code):
        self.req("command=returnTradeHistory&currencyPair=%s" % code)


    def transactionsInInterval(self, pairId, code, timeFrom, timeTo = None, idTo = None):
        start = timeFrom
        if timeTo:
            end = timeTo
        else:
            end = now()

        while start <= end:
            log("Retrieving trade history till %s..." % formatTime(end), LogType.BEGIN)
            trans = self.req("command=returnTradeHistory&currencyPair=%s&start=%i&end=%i" % (code, max(start, end - MONTH), end))
            log("%i txs retrieved" % len(trans), LogType.END)

            sql = "INSERT INTO trans (exchange_id, pair_id, trans_id, time, dir, price, vol) VALUES (%s, %s, %s, %s, %s, %s, %s) ON DUPLICATE KEY UPDATE dir = dir"
            data = []
            for tx in trans:
                data.append((self.exchangeId, pairId, tx["tradeID"], tx["date"], tx["type"], float(tx["rate"]), float(tx["amount"])))
            rowCount = self.db.insert(sql, data, multi=True)
            log("%i txs inserted" % rowCount)
            if rowCount < 1000: break
            end = parseTime(trans[-1]["date"])


    def transactions(self, code, start, options):
        return Gather.transactions(self, code.replace("-", "_"), start, options)


gather = PoloniexGather()
gather.run()

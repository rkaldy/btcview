#!/usr/bin/python3

import requests, json, time, hmac, hashlib, optparse

API_URL="https://www.bitstamp.net/api/v2/"
USER_ID = "013084"
KEY = "lo3l4cLWTDE4eW1Gfc89TKpZLowYOduM"
SECRET = "uxvXX361H0jk3Ugo1cUbhbkiCXimdgqG"
CURRS = ["btc", "bch", "ltc", "eth", "xrp"]


def req(cmd, param = ""):
    nonce = str(int(time.time()))
    msg = nonce + USER_ID + KEY
    sig = hmac.new(SECRET.encode(), msg.encode(), hashlib.sha256).hexdigest().upper()
    try:
        url = API_URL + cmd + "/" + param
        data = requests.post(url, data = { "key": KEY, "signature": sig, "nonce": nonce, "limit": 100 })
        data = data.json()
    except json.decoder.JSONDecodeError as e:
        raise Exception(data.text)
    if "status" in data and data["status"] == "error":
        raise Exception(data["reason"])
    return data


def show_fee():
    balance = req("balance")
    for curr in CURRS:
        pair = curr + "usd"
        ticker = req("ticker", pair)
        fee = float(balance[pair+"_fee"])
        inc = 2 * fee/100 * float(ticker["last"])
        print("%s: %.2f%% / %.2g" % (curr.upper(), fee, inc))


def show_trans():
    trans = req("user_transactions")
    deals = []
    actual = { c: { "balance": 0, "buyUSD": 0, "sellUSD": 0, "fee": 0, "buyTrans": {}, "sellTrans": {} } for c in CURRS }
    for tx in trans:
        if tx["type"] != "2": continue
        for c in CURRS:
            if c in tx and tx[c] != 0.0: break
        amount = float(tx[c])
        if amount > 0:
            dir = "buy"
        else:
            dir = "sell"
        actual[c]["balance"] += amount
        actual[c][dir + "USD"] += abs(float(tx["usd"]))
        actual[c]["fee"] += float(tx["fee"])
        rate = float(tx[c + "_usd"])
        if rate in actual[c][dir + "Trans"]:
            actual[c][dir + "Trans"][rate] += abs(amount)
        else:
            actual[c][dir + "Trans"][rate] = abs(amount)
        #print(actual[c])
        if abs(actual[c]["balance"]) < 1e-6:
            actual[c]["currency"] = c
            actual[c]["date"] = tx["datetime"]
            deals.append(actual[c])
            actual[c] = { "balance": 0, "buyUSD": 0, "sellUSD": 0, "fee": 0, "buyTrans": {}, "sellTrans": {} }

    for d in deals:
        print()
        print(d["date"] + ":")
        print("  Buys:")
        for rate in d["buyTrans"]:
            print("    % 7.4g %s for % 7.4g %s/USD" % (d["buyTrans"][rate], d["currency"].upper(), rate, d["currency"].upper()))
        print("  Sells:")
        for rate in d["sellTrans"]:
            print("    % 7.4g %s for % 7.4g %s/USD" % (d["sellTrans"][rate], d["currency"].upper(), rate, d["currency"].upper()))
        print("  Buy %.2f USD, sell %.2f USD" % (d["buyUSD"], d["sellUSD"]))
        y = d["sellUSD"] - d["buyUSD"]
        print("  Yield: %.2f USD / %.2f%%" % (y, y / d["buyUSD"] * 100))


parser = optparse.OptionParser()
parser.add_option("-f", "--fee", dest="fee", action="store_true", help="For each currency show transaction fee and minimum price increase to pay off the fees") 
parser.add_option("-t", "--transactions", dest="trans", action="store_true", help="Show all closed transactions")
(opt, args) = parser.parse_args()

try:
    if (opt.fee):
        show_fee()
    elif (opt.trans):
        show_trans()
    else:
        parser.print_help()
except Exception as e:
    print(e)


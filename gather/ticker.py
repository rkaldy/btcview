#!/usr/bin/python3 

import requests, json, collections, threading

rates = {}

class ReqThread(threading.Thread):
    def __init__(self, fun, arg):
        threading.Thread.__init__(self)
        self.fun = fun
        self.arg = arg

    def run(self):
        self.fun.__call__(self.arg)


def fixer(currs):
    global rates
    data = requests.get("http://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/denni_kurz.txt").text
    lines = data.split("\n")[2:-1]
    for line in lines:
        fields = line.split("|")
        curr = fields[3]
        if curr in currs:
            rates[curr] = float(fields[4].replace(",", "."))

def cryptowatch(_unused):
    global rates
    data = requests.get("https://api.cryptowat.ch/markets/kraken/usdtusd/price").json()
    rates["USDT"] = data["result"]["price"]

def bittrex(curr):
    global rates
    data = requests.get("https://bittrex.com/api/v1.1/public/getticker?market=USDT-" + curr).json()
    rates[curr] = data["result"]["Bid"]

def bitstamp(_unused):
    global rates
    data = requests.get("https://www.bitstamp.net/api/v2/ticker/btcusd").json()
    rates["BTC"] = float(data["bid"])

def bitfinex(currs):
    global rates
    data = requests.get("https://api.bitfinex.com/v2/tickers?symbols=" + ",".join(["t%sBTC" % c for c in currs])).json()
    for i in range(len(currs)):
        rates[currs[i]] = data[i][1]

def simplecoin(_unused):
    global rates
    data = requests.get("https://simplecoin.cz/ticker").json()
    rates["BTC"] = data["offer"]


def index(req):
    requests = [ ReqThread(fixer, ["EUR", "USD"]), ReqThread(bitstamp, None), ReqThread(bitfinex, ["BCH", "LTC", "ETH", "XMR", "ZEC"]) ]

    for req in requests:
        req.start()
    for req in requests:
        req.join()

    ret = "<html><body><table>"
    for curr in sorted(rates):
        ret += "<tr><td>%s</td><td>%.9g</td></tr>" % (curr, rates[curr])
    ret += "</table></body></html>"
    return ret

print("Content-Type: text/html")
print()
print(index(None))

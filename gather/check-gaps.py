#!/usr/bin/python3

import sys
from gather import *


if len(sys.argv) != 3:
    print("Usage: %s <exchange> <pair>" % sys.argv[0])
    sys.exit(1)

db = DB()
exchangeId = db.getExchange(sys.argv[1])
if not exchangeId:
    print("Unknown exchange: " + sys.argv[1])
    sys.exit(1)
pairId = db.getPair(sys.argv[2])
if not pairId:
    print("Unknown pair: " + sys.argv[2])
    sys.exit(1)

lastId = None
lastTime = None
gapFound = False
processed = 0
cur = db.cursor()
cur.execute("SELECT trans_id, time FROM trans WHERE exchange_id = %s AND pair_id = %s ORDER BY trans_id" % (exchangeId, pairId))
for tx in cur:
    transId = tx[0]
    transTime = tx[1]
    if lastId and transId != lastId + 1:
        print("Gap #%i - %i (%s - %s)" % (lastId, transId, lastTime.strftime(TIME_FORMAT), transTime.strftime(TIME_FORMAT)))
        gapFound = True
    lastId = transId
    lastTime = transTime
    processed += 1
    if processed % 200000 == 0:
        print("%s txs processed" % processed)

if processed == 0:
    print("No transactions in database")
elif not gapFound:
    print("No gaps")

#!/bin/bash

echo "Reading data from remote db..."
#ssh paranet "mysqldump --no-create-info btc2 | bzip2 > dump.sql.bz2" || exit 1

echo "Downloading data..."
#scp paranet:dump.sql.bz2 . || exit 1
#ssh paranet "rm dump.sql.bz2"

echo "Writing data to local db..."
bunzip2 -c dump.sql.bz2 | sed "s/INSERT/INSERT IGNORE/" | mysql -u user --password=password -D btc2 || exit 1
rm dump.sql.bz2

echo "Finished!"

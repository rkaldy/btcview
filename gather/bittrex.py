#!/usr/bin/python3

import requests, json
import MySQLdb as my
from datetime import datetime
from gather import *


API_URL="https://bittrex.com/api/v1.1/public/"
PAIR_REFRESH_INTERVAL=86400*7
MIN_DAILY_VOLUME=3000000
BITTREX_TIME_FORMAT="%Y-%m-%dT%H:%M:%S"


def req(cmd):
    data = requests.get(API_URL + cmd).json()
    if data["success"]:
        return data["result"]
    else:
        raise Exception(data["message"])


def parseBittrexTime(timeStr):
    return datetime.strptime(timeStr.split(".")[0], BITTREX_TIME_FORMAT).timestamp()


lastRefreshTime = 0
backupBaseToUSDT = { "USDT": 1, "BTC": 10000, "LTC": 88, "ETH": 450 }

def refreshPairs(db, pairs):
    global lastRefreshTime
    global backupBaseToUSDT
    if now() < lastRefreshTime + PAIR_REFRESH_INTERVAL: return

    log("Refreshing pair list")
    pairs.clear()
    baseToUSDT = { "USDT" : 1 }
    newpairs = req("getmarketsummaries")
    for pair in newpairs:
        code = pair["MarketName"]
        base = code.split("-")[0]
        if base not in baseToUSDT:
            ticker = req("getticker?market=USDT-" + base)
            if ticker and ticker["Last"]:
                baseToUSDT[base] = ticker["Last"]
            else:
                baseToUSDT[base] = backupBaseToUSDT[base]
        if pair["BaseVolume"] * baseToUSDT[base] < MIN_DAILY_VOLUME:
            continue
        log(" %s" % code)
        pairId = db.getPair(code)
        if not pairId:
            pairId = db.insert("INSERT INTO pair (code) VALUES (%s)", code)
        pairs[code] = Pair(pairId)
   
    lastRefreshTime = now()
    return


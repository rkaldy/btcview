#include <cmath>
#include <climits>
#include "stats.h"
#include <iostream>


void range(const vector<double>& data, double& dmin, double& dmax) {
	dmin = INT_MAX;
	dmax = INT_MIN;
	for (double x : data) {
		if (x < dmin) dmin = x;
		if (x > dmax) dmax = x;
	}
}


void range(const vector<vector<double>>& data, double& dmin, double& dmax) {
	dmin = INT_MAX;
	dmax = INT_MIN;
	for (const vector<double>& v : data) {
		for (double x : v) {
			if (x < dmin) dmin = x;
			if (x > dmax) dmax = x;
		}
	}
}


vector<double> histogram(const vector<double>& data, int bar_count, double dmin, double dmax) {
	if (dmin == 0 && dmax == 0) {
		range(data, dmin, dmax);
	}
	vector<double> hist(bar_count);
	for (double x : data) {
		int i = truncl((double)bar_count * (x - dmin) / (dmax - dmin));
		if (i == bar_count) i--;
		hist[i]++;
	}
	return hist;
}

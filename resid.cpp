#include <cmath>
#include <algorithm>
#include <iostream>
#include <parallel.h>
#include "bgraph.h"
#include "smoother.h"

using namespace std;


const int FILTER_COUNT 			= 15;
const int FILTER_DEG 			= 3;
const double FILTER_MIN_LEN 	= 0.5;
const double FILTER_MULTIPLIER	= 1.2;


class resid_graph : public mgraph {
public:
	resid_graph(sdlex& s, const market& m):
		mgraph(s, m, true, 0), 
		sm(m, FILTER_COUNT, FILTER_DEG, FILTER_MIN_LEN, FILTER_MULTIPLIER)
	{
		sm.pivot_limits(0, x_min, x_max);
		x = x_min;

		add_window(0, 1, false, 0.6);
		get_window(0).zoom_to(3);
		adjust_residue_zoom();
		recompute();
		
		register_handler(SDLK_LEFT, KMOD_ALT, this, &resid_graph::change_pivot);
		register_handler(SDLK_RIGHT, KMOD_ALT, this, &resid_graph::change_pivot);
		register_handler(SDLK_PAGEUP, KMOD_ALT, this, &resid_graph::change_pivot);
		register_handler(SDLK_PAGEDOWN, KMOD_ALT, this, &resid_graph::change_pivot);
	}

	bool change_pivot(SDL_Event& event) {
		switch (event.key.keysym.sym) {
			case SDLK_LEFT: 	x--; break;
			case SDLK_RIGHT: 	x++; break;
			case SDLK_PAGEUP: 	x -= 20; break;
			case SDLK_PAGEDOWN:	x += 20; break;
			default: break;
		}
		if (x < x_min) x = x_min;
		else if (x > x_max) x = x_max;

		recompute();

		return true;
	}

	void adjust_residue_zoom() {
		vector<double> rs(x_max - x_min + 1, 0);
		parallel::cycle(0, x_min, x_max + 1, [this, &rs](int x) {
			rs[x - x_min] = sm.band_residual_sum(0, x, sm.filter_count() - 1);			
		});
		sort(rs.begin(), rs.end());
		get_window(1).get_pane(0).set_zoom_y(0, rs[rs.size() * 4/5]);
	}

	void recompute() {
		vector<VectorXd> rc = sm.regression_curve(0, x);
		for (int i = 0; i < sm.filter_count(); i++) {
			int c = i * 128 / sm.filter_count();
			get_pane(0, 0).set_series(i, to_std_vector(rc[i]), sdl_color(0, c, 128));
		}
		get_pane(1, 0).set_series(0, sm.residue(0, x, true), BLUE);
		get_window(0).shift_x_to(x, true);
	}

	void draw() {
		mgraph::draw();
		pane& p1 = get_pane(0, 0);
		screen.dashed_vline(p1.xtog(x), p1.get_oy(), p1.get_ey(), BLACK, 3);
		pane& p2 = get_pane(0, 1);
		screen.dashed_vline(p2.xtog(x), p2.get_oy(), p2.get_ey(), BLACK, 3);
	}


	int x, x_min, x_max;
	smoother sm;
};


int main(int argc, char* argv[]) {
	cout << "Reading data" << endl;
	const char* src = argc > 1 ? argv[1] : "ticker_USDT-BTC_30v.dat";
	market m(src, true);
	
	cout << "Displaying graph" << endl;
	bsdlex s(argc, argv);
	resid_graph g(s, m);
	g.run();
}

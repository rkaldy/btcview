#include <iostream>
#include "market.h"
#include "bgraph.h"

using namespace std;

int main(int argc, char** argv) {
	if (argc != 2) {
		cerr << "Usage: " << argv[0] << " <data-file>" << endl;
		return 1;
	}

	market m(argv[1]);
	bsdlex s(true);
	mgraph g(s, m, true);
	g.run();
}

#include <cmath>
#include <cassert>
#include "filter.h"


filter::filter(int alen, int aoff):
	len(alen),
	off(aoff)
{
	assert(off >= 0 && off < len);
}


double filter::smooth_point(const VectorXd& src, int x) const {
	assert(x >= off);
	assert(x-off+len <= (int)src.size());

	return src.segment(x-off, len).dot(coef);
}


VectorXd filter::smooth(const VectorXd& src) const {
	VectorXd sm(src.size());
	
	sm.head(off).setConstant(NAN);
	for (int x = off; x <= src.size()-len+off; x++) {
		sm[x] = smooth_point(src, x);
	}
	sm.tail(len-off-1).setConstant(NAN);
	return sm;
}


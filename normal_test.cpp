#include "test.h"
#include "normal.h"


TEST(normal, dens) {
	EXPECT_FLOAT_EQ(normal::dens(1),		0.241970724);
	EXPECT_FLOAT_EQ(normal::dens(-2, 1),	0.00443184841);
	EXPECT_FLOAT_EQ(normal::dens(-2, 1, 5),	0.0666449206);
}

TEST(normal, dist) {
	EXPECT_FLOAT_EQ(normal::dist(1),		0.841344746);
	EXPECT_FLOAT_EQ(normal::dist(-2, 1),	0.00134989803);
	EXPECT_FLOAT_EQ(normal::dist(-2, 1, 5),	0.274253118);
}

TEST(normal, quant) {
	EXPECT_FLOAT_EQ(normal::quant(0.2),		 -0.84162122);
	EXPECT_FLOAT_EQ(normal::quant(0.7, 1),	  1.52440054);
	EXPECT_FLOAT_EQ(normal::quant(0.7, 1, 5), 3.62200272);
}

TEST(normal, shapiro_wilk) {
	VectorXd x(12);
	x << 65, 61, 63, 86, 70, 55, 74, 35, 72, 68, 45, 58;
	EXPECT_NEAR(normal::shapiro_wilk(x), 0.9216489, 1e-6);
	x.setLinSpaced(100, 1, 100);
	EXPECT_NEAR(normal::shapiro_wilk(x), 0.001721722, 1e-8);
}

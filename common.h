#ifndef COMMON_H
#define COMMON_H

#include <iostream>

using namespace std;


#ifdef NDEBUG
	#define LOG(msg)
#else
	#define LOG(msg) cout << msg << endl;
#endif

#ifdef LOG_TRACE
	#define TRACE(msg) cout << msg << endl;
#else
	#define TRACE(msg)
#endif

#define PAR(var) #var << "=" << var << " "

#endif  // COMMON_H

#include "test.h"
#include "savitzky_golay.h"


TEST(savitzky_golay, quadratic) {
	VectorXd v1_exp(5);	 		v1_exp		<< 1, 1, 1, 1, 1;
	RowVectorXd coef_exp(5);	coef_exp	<< -3.0/35, 12.0/35, 17.0/35, 12.0/35, -3.0/35;
	RowVectorXd vplus2_exp(5);	vplus2_exp	<< -0.2, -0.1, 0, 0.1, 0.2;
	RowVectorXd cp1_exp(5);		cp1_exp		<< 3.0/5, -3.0/5, -4.0/5, 0.0/5, 9.0/5;
	RowVectorXd cp2_exp(5);		cp2_exp		<< 7.0/5, -6.0/5, -9.0/5, -2.0/5, 15.0/5;

	savitzky_golay f(2, 5, 2, 3);

	EXPECT_VECTORS_NEAR(f.v.col(0), v1_exp);
	EXPECT_VECTORS_NEAR(f.coef, coef_exp);
	EXPECT_VECTORS_NEAR(f.vplus.row(1), vplus2_exp);
	EXPECT_VECTORS_NEAR(f.cp.row(0), cp1_exp);
	EXPECT_VECTORS_NEAR(f.cp.row(1), cp2_exp);
}


TEST(savitzky_golay, cubic) {
	VectorXd v4_exp(5);	 		v4_exp		<< -8, -1, 0, 1, 8;
	RowVectorXd coef_exp(5);	coef_exp	<< -3.0/35, 12.0/35, 17.0/35, 12.0/35, -3.0/35;
	RowVectorXd vplus2_exp(5);	vplus2_exp	<< 1.0/12, -8.0/12, 0, 8.0/12, -1.0/12;

	savitzky_golay f(3, 5, 2);
	
	EXPECT_VECTORS_NEAR(f.v.col(3), v4_exp);
	EXPECT_VECTORS_NEAR(f.coef, coef_exp);
	EXPECT_VECTORS_NEAR(f.vplus.row(1), vplus2_exp);
}


TEST(savitzky_golay, weighted_quadratic) {
	VectorXd weight(5);			weight		<< 1, 2, 3, 2, 1;
	VectorXd v1_exp(5);	 		v1_exp		<< 1, 1, 1, 1, 1;
	RowVectorXd coef_exp(5);	coef_exp	<< -1.0/15, 4.0/15, 9.0/15, 4.0/15, -1.0/15;
	RowVectorXd vplus2_exp(5);	vplus2_exp	<< -5.0/30, -5.0/30, 0, 5.0/30, 5.0/30;

	savitzky_golay f(2, 5, 2, weight);
	
	EXPECT_VECTORS_NEAR(f.v.col(0), v1_exp);
	EXPECT_VECTORS_NEAR(f.coef, coef_exp);
	EXPECT_VECTORS_NEAR(f.vplus.row(1), vplus2_exp);
}


TEST(savitzky_golay, exp_weighted) {
	VectorXd weight_exp(11);		weight_exp	<< 1.0/32, 1.0/16, 1.0/8, 1.0/4, 1.0/2, 1.0, 1.0/2, 1.0/4, 1.0/8, 1.0/16, 1.0/32;
	weight_exp /= weight_exp.sum();

	savitzky_golay f(2, 1.0, 0.5);
	
	EXPECT_EQ(f.len, 11);
	EXPECT_EQ(f.off, 5);
	EXPECT_VECTORS_NEAR(f.w, weight_exp);
}


TEST(savitzky_golay, exp_weighted_asymetric) {
	VectorXd weight_left_exp(9);	weight_left_exp	<< 1.0/32, 1.0/16, 1.0/8, 1.0/4, 1.0/2, 1.0, 1.0/2, 1.0/4, 1.0/8;
	VectorXd weight_right_exp(9);	weight_right_exp<< 1.0/8, 1.0/4, 1.0/2, 1.0, 1.0/2, 1.0/4, 1.0/8, 1.0/16, 1.0/32;
	weight_left_exp /= weight_left_exp.sum();
	weight_right_exp /= weight_right_exp.sum();

	savitzky_golay fl(2, 1.0, 0.375);
	
	EXPECT_EQ(fl.len, 9);
	EXPECT_EQ(fl.off, 3);
	EXPECT_VECTORS_NEAR(fl.w, weight_left_exp);

	savitzky_golay fr(2, 1.0, 0.625);
	
	EXPECT_EQ(fr.len, 9);
	EXPECT_EQ(fr.off, 5);
	EXPECT_VECTORS_NEAR(fr.w, weight_right_exp);
}


TEST(savitzky_golay, smooth_point_residual_sum_predict) {
	VectorXd x(5);	 		x		 << 1, 5, 6, 3, 2;
	VectorXd pred_exp(3);	pred_exp << -3.6, -10.6, -19.6;

	savitzky_golay f(2, 5, 2, 3);
	double s = f.smooth_point(x, 2);
	double rs = f.residual_sum(x, 2);
	VectorXd pred = f.predict(x, 2);

	EXPECT_DOUBLE_EQ(s, 5.4);
	EXPECT_DOUBLE_EQ(rs, 0.64);
	EXPECT_VECTORS_NEAR(pred, pred_exp);
}


TEST(savitzky_golay, smooth) {
	VectorXd x(7);	 		x		<< 1, 3, 6, 4, 5, 3, 2;
	VectorXd sm_exp(7); 	sm_exp	<< NAN, NAN, 168.0/35, 182.0/35, 145.0/35, NAN, NAN;
	
	savitzky_golay f(2, 5, 2);
	VectorXd sm = f.smooth(x);

	EXPECT_VECTORS_NEAR(sm, sm_exp);
}


TEST(savitzky_golay, regression_curve)
{
	VectorXd x(5);		x 	   << 1, 5, 6, 3, 2;
	VectorXd rc_exp(5);	rc_exp << 1.4, 4.4, 5.4, 4.4, 1.4;

	savitzky_golay f(2, 5, 2);
	VectorXd rc = f.regression_curve(x, 2);

	EXPECT_VECTORS_NEAR(rc, rc_exp);
}

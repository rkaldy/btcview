#include <cassert>
#include <cmath>
#include <algorithm>
#include <itpp/itbase.h>
#include "stats.h"
#include "normal.h"


double normal::dens(double x, double mu, double sigma) {
	assert(sigma > 0);
	x = (x - mu) / sigma;
	return exp(-x*x / 2) / (sqrt(2 * M_PI) * sigma);
}


double normal::dist(double x, double mu, double sigma) {
	assert(sigma > 0);
	x = (x - mu) / sigma;
	return (1 + erf(x / M_SQRT2)) / 2;
}


double normal::quant(double p, double mu, double sigma) {
	assert(sigma > 0);
	assert(p > 0 && p < 1);
	return mu + sigma * M_SQRT2 * itpp::erfinv(2*p - 1);
}


double normal::shapiro_wilk(VectorXd& x) {
	assert(x.size() >= 12);
	
	int n = x.size();
	sort(x.data(), x.data() + n);
	
	VectorXd m(n);
	for (int i = 0; i < n; i++) {
		m[i] = quant((i + 0.625) / (n + 0.25));
	}
	double m2 = m.norm();

	VectorXd a(n);
	double u = 1 / sqrt(n);
	a[0] = 2.706056*pow(u, 5) - 4.434685*pow(u, 4) + 2.071190*pow(u, 3) + 0.147981*pow(u, 2) - 0.221157*u + m[0] / m2;
	a[1] = 3.582633*pow(u, 5) - 5.682633*pow(u, 4) + 1.752461*pow(u, 3) + 0.293762*pow(u, 2) - 0.042981*u + m[1] / m2;
	double eps = sqrt((m2*m2 - 2*m[0]*m[0] - 2*m[1]*m[1]) / (1 - 2*a[0]*a[0] - 2*a[1]*a[1]));
	a.segment(2, n-4) = m.segment(2, n-4) / eps;
	a[n-2] = -a[1];
	a[n-1] = -a[0];

	double W = a.dot(x);
	W = W*W;
	x = x.array() - x.mean();
	W /= x.squaredNorm();

	double l = log(n);
	double w_mu = 0.0038915*pow(l, 3) - 0.083751*pow(l, 2) - 0.31082*l - 1.5861;
	double w_sigma = exp(0.0030302*pow(l, 2) - 0.082676*l - 0.4803);
	return 1 - dist(log(1 - W), w_mu, w_sigma);
}

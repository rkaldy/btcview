#include "test.h"
#include "time.h"


TEST(time, format) {
		string s = format_time(1526891411);
		ASSERT_EQ(s, string("21.05.2018 08:30:11"));
}

TEST(time, parse) {
	time_t t = parse_time("21.5.2018 8:30:11");
	ASSERT_EQ(t, 1526891411);
	t = parse_time("27.8.2018");
	ASSERT_EQ(t, 1535328000);
}

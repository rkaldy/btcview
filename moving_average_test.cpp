#include "test.h"
#include "moving_average.h"


TEST(moving_average, filter) {
	VectorXd coef_exp(5);	coef_exp.setConstant(0.2);
	moving_average f(5, 2);
	EXPECT_VECTORS_NEAR(f.coef, coef_exp);
}

TEST(moving_average, smooth) {
	VectorXd x(7);	 		x		<< 1, 3, 6, 4, 5, 3, 2;
	VectorXd sm_exp(7); 	sm_exp	<< NAN, NAN, 19.0/5, 21.0/5, 20.0/5, NAN, NAN;
	VectorXd sm = moving_average::smooth_simple(x, 5, 2);
	EXPECT_VECTORS_NEAR(sm, sm_exp);
}

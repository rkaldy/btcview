#include <cassert>
#include <cmath>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include "market.h"


quote::quote() {
	open = high = low = close = high_perc = low_perc = avg = vol = NAN;
}


market::market() : start(0), step(0)
{}


market::market(const char* file, bool logarithmic) {
	load(file, logarithmic);
}


template <typename T>
void bread(ifstream& f, T& data) {
	f.read((char*)&data, sizeof(T));
}

void market::load(const char* file, bool logarithmic) {
	ifstream f(file, ios::binary);
	if (f.fail()) {
		stringstream ss;
		ss << "File not found: " << file;
		throw runtime_error(ss.str());
	}

	bread(f, start);
	bread(f, step);

	chunks.emplace_back();
	chunk* ch = &chunks.back();
	quote q;
	while (true) {
		bread(f, q);
		if (f.eof()) break;
		if (isnan(q.open)) {
			ch->gap = ((gap*)&q)->len;
			chunks.emplace_back();
			ch = &chunks.back();
			continue;
		} 
		if (logarithmic) {
			q.open		= log(q.open);
			q.high		= log(q.high);
			q.low		= log(q.low);
			q.close     = log(q.close);
			q.high_perc	= log(q.high_perc);
			q.low_perc	= log(q.low_perc);
			q.avg		= log(q.avg);	
		}
		ch->quotes.push_back(q);
	}
}


int market::ticker_count(bool with_gaps) const {
	int count = 0;
	for (const chunk& ch: chunks) {
		count += ch.quotes.size();
		if (with_gaps) {
			count += ch.gap;
		}
	}
	return count;
}


vector<ticker_t> market::ticker(size_t ch) const {
	assert(ch < chunks.size());
	vector<ticker_t> ret;
	ret.reserve(chunks[ch].quotes.size());
	for (const quote& q: chunks[ch].quotes) {
		ret.push_back((ticker_t&)q);
	}
	return ret;
}


vector<ticker_t> market::ticker() const {
	vector<ticker_t> ret;
	ret.reserve(ticker_count(true));

	for (const chunk& ch: chunks) {
		for (const quote& q: ch.quotes) {
			ret.push_back((ticker_t&)q);
		}
		for (int i = 0; i < ch.gap; i++) {
			ret.push_back({ NAN, NAN, NAN, NAN });
		}
	}
	return ret;
}


VectorXd market::extract(size_t chunk, double quote::* field) const {
	assert(chunk >= 0 && chunk < chunks.size());
	const vector<quote>& q = chunks[chunk].quotes;
	VectorXd ret(q.size());
	for (size_t i = 0; i < q.size(); i++) {
		ret[i] = q[i].*field;
	}
	return ret;
}


vector<double> market::extract(double quote::* field) const {
	vector<double> ret;
	ret.reserve(ticker_count(true));

	for (const chunk& ch: chunks) {
		for (const quote& q: ch.quotes) {
			ret.push_back(q.*field);
		}
		for (int i = 0; i < ch.gap; i++) {
			ret.push_back(NAN);
		}
	}
	return ret;
}

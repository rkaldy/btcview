#include <stdexcept>
#include <sstream>
#include <cstring>
#include "time.h"

#define TIME_FORMAT "%d.%m.%Y %H:%M:%S"
#define DATE_FORMAT "%d.%m.%Y"


string format_time(time_t t) {
	static char buf[256];
	struct tm* tm = gmtime(&t);
	size_t len = ::strftime(buf, 255, TIME_FORMAT, tm);
	return string(buf, len);
}


time_t parse_time(const string& s) {
	static struct tm tm;
	::memset(&tm, 0, sizeof(struct tm));
	if (::strptime(s.c_str(), TIME_FORMAT, &tm)) {
		return ::timegm(&tm);
	}
	if (::strptime(s.c_str(), DATE_FORMAT, &tm)) {
		return ::timegm(&tm);
	}
	stringstream ss;
	ss << "Unparseable time " << s;
	throw runtime_error(ss.str());
}

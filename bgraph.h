#include <sdlex.h>
#include <graph.h>
#include "market.h"


class bsdlex : public sdlex {
public:
	bsdlex(bool fullscreen = false);
	bsdlex(int argc, char* argv[]);

	operator sdlex() { return *(sdlex*)this; }
};


/*
 * Market ticker graph
 */
class mgraph : public graph {
public:
	/**
	 * Create ticker graph from a market instance
	 *
	 * @in sdlex		SDLEx graphics mode structure
	 * @in market		Market instance
	 * @in with_vol		Display volume in bottom pane
	 * @in chunk		Ticker chunk to display. If -1, then gaps will be displayed as NANs.
	 */
	mgraph(sdlex& screen, const market& market, bool with_vol = false, int chunk = -1);

	/**
	 * Add series from Eigen vector
	 *
	 * @in data			Y-values
	 * @in color		Series line color (SGE type)
	 */
	mgraph& add_series(const VectorXd& data, sdl_color color);

	/**
	 * Change existing series from Eigen vector
	 * If series_id == series.size() add a new series
	 *
	 * @in series_id	Series index
	 * @in data			Y-values
	 * @in color		Series line color (SGE type)
	 */
	mgraph& set_series(size_t series_id, const VectorXd& data, sdl_color color);

	/**
	 * Convert Eigen vector to std::vector
	 */
	static vector<double> to_std_vector(const VectorXd& vec);

protected:
	vector<ticker_t> ticker;
};


/**
 * Simple histogram graph
 */
class hgraph : public graph {
public:
	/**
	 * Create histogram from one data series
	 *
	 * @in sdlex		SDLEx graphics mode structure
	 * @in src			Input data
	 * @in bar_count	Total number of histogram bars
	 * @in bar_color	Bar color
	 */
	hgraph(sdlex& screen, vector<double>& src, int bar_count, sdl_color bar_color = BLUE);

	/**
	 * Create histograms from multiple data series. Toggle between theu by keys N and M.
	 *
	 * @in sdlex		SDLEx graphics mode structure
	 * @in src			Input data (vector of vectors)
	 * @in bar_count	Total number of histogram bars
	 * @in bar_color	Bar color
	 */
	hgraph(sdlex& screen, vector<vector<double>>& src, int bar_count, sdl_color bar_color = BLUE);

protected:
	bool toggle(SDL_Event& event);

	vector<vector<double>> data;
	int hist_idx;
	sdl_color bar_color;
};


/**
 * Function graph
 */
class fgraph : public graph {
public:
	/**
	 * Create function graph
	 *
	 * @in screen		SDLEx graphics mode structure
	 * @in fun			Plotted function (double->double)
	 * @in xmin, xmax	X-axis limits
	 * @in ticks		Number of ticks
	 * @color			Graph color
	 */
	fgraph(sdlex& screen, function<double(double)> fun, double xmin, double xmax, int ticks = 100, sdl_color color = GREEN);

protected:
	vector<double> y;
};

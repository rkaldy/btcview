#include <vector>

using namespace std;

/**
 * Find minimum and maximum of data
 *
 * @in data			Input data (vector)
 * @out dmin
 * @out dmax
 */
void range(const vector<double>& data, double& dmin, double& dmax);

/**
 * Find minimum and maximum of multiple data series
 *
 * @in data			Input data (vector of vectors)
 * @out dmin
 * @out dmax
 */
void range(const vector<vector<double>>& data, double& dmin, double& dmax);

/**
 * Create histogram from data series
 *
 * @in data			Input data (vector)
 * @in bar_count	Number of bars along the whole histogram
 * @in dmin, dmax	Histogram range (if zero, it will be computed from min/max of data)
 *
 * @return			Histogram data (<bar_count>-sized vector)
 */
vector<double> histogram(const vector<double>& data, int bar_count, double dmin = 0, double dmax = 0);


/**
 * Perform binary search to find the highest x in interval <start, end), such that f(x) <= threshold
 *
 * @in start		Lower bound for x
 * @in end			Upper bound for x
 * @in threshold 	Function value threshold
 * @in function		Evaluation function. Takes int as parameter and returns Value.
 * 					Should be monotonically increasing.
 *
 * @return			Highest x such that f(x) <= threshold
 */
template<typename Value, typename Function>
int binary_search(int start, int end, Value threshold, Function func) {
	while (end - start > 1) {
		int mid = (start + end + 1) / 2;
		Value mid_val = func(mid);
		if (mid_val <= threshold) {
			start = mid;
		} else {
			end = mid;
		}
	}
	return start;
}

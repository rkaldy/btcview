#include <ctime>
#include <string>

using namespace std;


string format_time(time_t t);
time_t parse_time(const string& s);

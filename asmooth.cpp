#include <cmath>
#include <iostream>
#include "bgraph.h"
#include "smoother.h"

using namespace std;


const int FILTER_COUNT 	 		= 70;
const int FILTER_DEG	 		= 3;
const double FILTER_MIN_LEN		= 0.5;
const double FILTER_MULTIPLIER	= 1.05;


class smooth_adaptive_graph : public mgraph {
public:
	smooth_adaptive_graph(sdlex& s, market& m, vector<double>& y, vector<double>& filter_len ) : 
		mgraph(s, m, false, 0)
	{
		graph::add_series(y, BLUE);
		get_window(0).add_pane(0.15).add_series(filter_len, RED);
		get_window(0).add_pane(0.15).add_series(m.vol(), BLUE, true);
	}
};


int main(int argc, char* argv[]) {
	cout << "Reading data" << endl;
	const char* src = argc > 1 ? argv[1] : "ticker_USDT-BTC_30v.dat";
	market m(src, true);
	
	smoother sm(m, FILTER_COUNT, FILTER_DEG, FILTER_MIN_LEN, FILTER_MULTIPLIER);
	vector<double> y, filter_len;
	sm.smooth_adaptive(0, 0.005, y, filter_len);
	
	cout << "Displaying graph" << endl;
	bsdlex s(argc, argv);
	smooth_adaptive_graph g(s, m, y, filter_len);
	g.run();
}

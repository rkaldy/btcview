CFLAGS = -std=c++14 -Wall `sdl-config --cflags` -I/usr/include/eigen3 -I/usr/include/mysql
CFLAGS_DEBUG = -g -DDEBUG
CFLAGS_RELEASE = -O3
LIBS = -lm -lpthread -lrt -litpp `sdl-config --libs` -lsdlex -lSDL_image -lSDL_ttf -lSGE -lboost_program_options -lmysqlclient -lmysqlpp
LIBS_DEBUG = -lparallel-dbg -lgraph-dbg
LIBS_RELEASE = -lparallel -lgraph

DATA_OBJS = db.o market.o time.o
BASIC_OBJS = filter.o savitzky_golay.o moving_average.o normal.o stats.o $(DATA_OBJS)
ALL_OBJS = $(BASIC_OBJS) bgraph.o smoother.o
TEST_OBJS = test.o time_test.o savitzky_golay_test.o moving_average_test.o normal_test.o market_test.o optimize_test.o stats_test.o $(BASIC_OBJS)

EXECUTABLES = display candle smooth resid asmooth test


ifeq ($(RELEASE), yes)
	CFLAGS += $(CFLAGS_RELEASE)
	LIBS += $(LIBS_RELEASE)
	BUILDDIR = release
else
	CFLAGS += $(CFLAGS_DEBUG)
	LIBS += $(LIBS_DEBUG)
	BUILDDIR = debug
endif


.PHONY: all
all: $(EXECUTABLES)

.PHONY: rebuild
rebuild: clean all

clean:
	rm -f debug/* release/* $(EXECUTABLES)

test: $(addprefix debug/, $(TEST_OBJS)) candle
	g++ -o test $(addprefix debug/, $(TEST_OBJS)) /usr/local/lib/libgtest.a $(LIBS)
	./test

display: $(addprefix $(BUILDDIR)/, $(ALL_OBJS)) $(BUILDDIR)/display.o
	g++ -o $@ $^ $(LIBS)

candle: $(addprefix $(BUILDDIR)/, $(DATA_OBJS)) $(BUILDDIR)/candle.o
	g++ -o $@ $^ $(LIBS) 

smooth: $(addprefix $(BUILDDIR)/, $(ALL_OBJS)) $(BUILDDIR)/smooth.o
	g++ -o $@ $^ $(LIBS)

resid: $(addprefix $(BUILDDIR)/, $(ALL_OBJS)) $(BUILDDIR)/resid.o
	g++ -o $@ $^ $(LIBS)

asmooth: $(addprefix $(BUILDDIR)/, $(ALL_OBJS)) $(BUILDDIR)/asmooth.o
	g++ -o $@ $^ $(LIBS)

pruba: pruba.cpp
	g++ $(CFLAGS) -o pruba pruba.cpp 

$(BUILDDIR)/%.o: %.cpp 
	g++ $(CFLAGS) -c -o $@ $<

#include <vector>
#include <mysql++/mysql++.h>

using namespace std;


class db {
private:
	mysqlpp::Connection conn;
	
	typedef enum { Exchange, Pair, size } Q;
	vector<mysqlpp::Query> builtin_query;
	static const char* BUILTIN_QUERY_SQL[Q::size];

public:
	/**
	 * Creates a database connection with hardcoded credentials
	 */
	db();

	/**
	 * Creates a query. Do use() or store() to run it and pass parameters
	 *
	 * @in sql		SQL query string with '%0q..%9q' as parameter placeholders
	 */
	mysqlpp::Query query(const char* sql);

	/**
	 * Execute a data manipulation statement without parameters
	 *
	 * @in sql		SQL statement
	 */
	mysqlpp::SimpleResult execute(const char* sql);

	/**
	 * Get exchange ID by its name
	 */
	int get_exchange_id(const char* name);

	/**
	 * Get pair ID by its code
	 */
	int get_pair_id(const char* code);
};	

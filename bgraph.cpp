#include <cassert>
#include <cstring>
#include <iostream>
#include "stats.h"
#include "bgraph.h"


// bsdlex

bsdlex::bsdlex(bool fullscreen) : sdlex(fullscreen) {
	add_font("regular.ttf", 10);
}


bsdlex::bsdlex(int argc, char* argv[]) : sdlex(!(argc > 1 && strcmp(argv[1], "-w") == 0)) {
	add_font("regular.ttf", 10);
}


// mgraph

mgraph::mgraph(sdlex& screen, const market& market, bool with_vol, int chunk) : 
	graph(screen, market.start, market.step, true)
{

	if (chunk == -1) {
		ticker = market.ticker();
	} else {
		ticker = market.ticker(chunk);
	}
	add_ticker(ticker);
	if (with_vol) {
		get_window(0).add_pane(0.2);
		get_pane(0, 1).add_series(market.vol(), BLUE, true);
	}
}


mgraph& mgraph::add_series(const VectorXd& data, sdl_color color) {
	graph::add_series(to_std_vector(data), color);
	return *this;
}


mgraph& mgraph::set_series(size_t sid, const VectorXd& data, sdl_color color) {
	graph::set_series(sid, to_std_vector(data), color);
	return *this;
}


vector<double> mgraph::to_std_vector(const VectorXd& v) {
	return vector<double>(v.data(), v.data() + v.size());
}


// hgraph

hgraph::hgraph(sdlex& screen, vector<double>& src, int bar_count, sdl_color a_bar_color) : 
	graph(screen), 
	hist_idx(0), 
	bar_color(a_bar_color) 
{
	assert(bar_count > 0);
	assert(!src.empty());

	double dmin, dmax;
	range(src, dmin, dmax);
	data.push_back(histogram(src, bar_count, dmin, dmax));

	add_window(dmin, (dmax - dmin) / bar_count);
	add_series(data[0], bar_color, true);
}


hgraph::hgraph(sdlex& screen, vector<vector<double>>& src, int bar_count, sdl_color a_bar_color) : 
	graph(screen), 
	hist_idx(0), 
	bar_color(a_bar_color) 
{
	assert(bar_count > 0);
	assert(!src.empty());

	double dmin, dmax;
	range(src, dmin, dmax);
	for (vector<double>& v : src) {
		data.push_back(histogram(v, bar_count, dmin, dmax));
	}

	add_window(dmin, (dmax - dmin) / bar_count);
	add_series(data[0], bar_color, true);

	register_handler(SDLK_m, KMOD_NONE, this, &hgraph::toggle);
	register_handler(SDLK_n, KMOD_NONE, this, &hgraph::toggle);
}


bool hgraph::toggle(SDL_Event& event) {
	if (event.key.keysym.sym == SDLK_m) hist_idx = (hist_idx + 1) % data.size();
	else if (event.key.keysym.sym == SDLK_n) hist_idx = (hist_idx + data.size() - 1) % data.size();
	set_series(0, data[hist_idx], bar_color, true);
	return true;
}


// fgraph

fgraph::fgraph(sdlex& screen, function<double(double)> fun, double xmin, double xmax, int ticks, sdl_color color) : 
	graph(screen), 
	y(ticks) 
{
	assert(ticks > 0);
	assert(xmax > xmin);

	double step = (xmax - xmin) / ticks;

	add_window(xmin, step);
	for (int i = 0; i < ticks; i++) {
		cout << xmin + i * step << endl;
		y[i] = fun(xmin + i * step);
	}
	add_series(y, color);
}

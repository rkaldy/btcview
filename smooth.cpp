#include <cmath>
#include <iostream>
#include "bgraph.h"
#include "smoother.h"

using namespace std;


const int FILTER_COUNT 	 		= 15;
const int FILTER_DEG	 		= 4;
const double FILTER_MIN_LEN		= 3.0;
const double FILTER_MULTIPLIER	= 1.2;


class smooth_graph : public mgraph {
public:
	smooth_graph(sdlex& s, market& m, smoother& a_sm, vector<VectorXd>& y) : 
		mgraph(s, m, false, 0),
		sm(a_sm)
	{
		get_window(0).add_pane(0.2).add_series(m.vol(), BLUE, true);

		sm.pivot_limits(0, x_min, x_max);
		x = x_min;
		
		for (int i = 0; i < sm.filter_count(); i++) {
			int c = i * 128 / sm.filter_count();
			add_series(y[i], sdl_color(c, c, c));
		}
		recompute();

		register_handler(SDLK_LEFT, KMOD_ALT, this, &smooth_graph::change_pivot);
		register_handler(SDLK_RIGHT, KMOD_ALT, this, &smooth_graph::change_pivot);
		register_handler(SDLK_PAGEUP, KMOD_ALT, this, &smooth_graph::change_pivot);
		register_handler(SDLK_PAGEDOWN, KMOD_ALT, this, &smooth_graph::change_pivot);
	}

	bool change_pivot(SDL_Event& event) {
		switch (event.key.keysym.sym) {
			case SDLK_LEFT: x--; break;
			case SDLK_RIGHT: x++; break;
			case SDLK_PAGEUP: x -= 40; break;
			case SDLK_PAGEDOWN: x += 40; break;
			default: break;
		}
		if (x < x_min) x = x_min;
		else if (x > x_max) x = x_max;
		recompute();
		return true;
	}

	void recompute() {
		vector<VectorXd> rc = sm.regression_curve(0, x);
		for (int i = 0; i < sm.filter_count(); i++) {
			int c = i * 128 / sm.filter_count();
			set_series(sm.filter_count() + i, rc[i], sdl_color(0, c, c));
		}
	}

	smoother& sm;
	int x, x_min, x_max;
};


int main(int argc, char* argv[]) {
	cout << "Reading data" << endl;
	const char* src = argc > 1 ? argv[1] : "ticker_USDT-BTC_30v.dat";
	market m(src, true);
	
	smoother sm(m, FILTER_COUNT, FILTER_DEG, FILTER_MIN_LEN, FILTER_MULTIPLIER);
	vector<VectorXd> y = sm.smooth(0);
	
	cout << "Displaying graph" << endl;
	bsdlex s(argc, argv);
	smooth_graph g(s, m, sm, y);
	g.run();
}

#include "test.h"
#include "stats.h"


TEST(stats, histogram) {
	vector<double> data = { 4.9, 2, 7, 1, 3, 2 };
	vector<double> hist_exp1 = { 3, 2, 1 };
	vector<double> hist_exp2 = { 5, 1, 0 };

	vector<double> hist = histogram(data, 3);
	EXPECT_VECTORS_NEAR(hist, hist_exp1);

	hist = histogram(data, 3, 0, 15);
	EXPECT_VECTORS_NEAR(hist, hist_exp2);
}


TEST(stats, binary_search) {
	int sqrt = binary_search(0, 200, 2.0, [](int x){ return x * x * 1e-4; });
	EXPECT_EQ(sqrt, 141);
	
	sqrt = binary_search(0, 200, 0.0, [](int x){ return x * x * 1e-4; });
	EXPECT_EQ(sqrt, 0);
	
	sqrt = binary_search(0, 200, 9999.0, [](int x){ return x * x * 1e-4; });
	EXPECT_EQ(sqrt, 199);
}


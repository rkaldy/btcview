#ifndef MARKET_H
#define MARKET_H

#include <vector>
#include <graph.h>
#include <Eigen/Dense>


using namespace std;
using namespace Eigen;


class quote : public ticker_t {
public:
	quote();
	
	double vol;
};

class chunk {
public:
	chunk() : gap(0)	{}		

	vector<quote> quotes;
	int gap;
};

class gap {
public:
	gap(int a_len) : _pad1(NAN), len(a_len)	{}

	double _pad1;
	int len;
	char _pad2[sizeof(quote) - sizeof(double) - sizeof(int)];
};


class market {
public:
	vector<chunk> chunks;
	unsigned int start, step;

	/**
	 * Initializes an empty market instance
	 */
	market();

	/**
	 * Initializes and load data from binary file
	 *
	 * @in file			Data file name
	 * @in logarithmic	Read logs of prices instead of their absolute values
	 */
	market(const char* file, bool logarithmic = false);

	/**
	 * Load data from binary file
	 *
	 * @in file			Data file name
	 * @in logarithmic	Read logs of prices instead of their absolute values
	 */
	void load(const char* file, bool logarithmic = false);

	/**
	 * Get number of consecutive chunks
	 */
	int size() const	{ return chunks.size(); }

	/**
	 * Get total ticker count
	 *
	 * @in with_gaps	if true the gaps will be replaced by NAN tickers
	 */
	int ticker_count(bool with_gaps) const;

	/**
	 * Get vector of tickers, useful for displaying in a graph
	 * If chunk not specified, output gaps as tickers with NANs
	 *
	 * @in chunk	Chunk number (optional)
	 */
	vector<ticker_t> ticker(size_t chunk) const;
	vector<ticker_t> ticker() const;

	/**
	 * Get one quote field (open, high, low, close, high_perc, low_perc, avg, vol) from one consecutive chunk as Eigen vector
	 *
	 * @in chunk	Chunk number
	 * @in field	Field pointer-to-member
	 */
	VectorXd extract(size_t chunk, double quote::* field) const;

	/**
	 * Get one quote field (open, high, low, close, high_perc, low_perc, avg, vol) as std::vector.
	 * Output gaps as NANs
	 */
	vector<double> extract(double quote::* field) const;

	VectorXd avg(size_t chunk) const	{ return extract(chunk, &quote::avg); }
	VectorXd vol(size_t chunk) const	{ return extract(chunk, &quote::vol); }

	vector<double> avg() const		{ return extract(&quote::avg); }
	vector<double> vol() const		{ return extract(&quote::vol); }
};


#endif  // MARKET_H

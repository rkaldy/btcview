#include <climits>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <stdexcept>
#include <boost/program_options.hpp>
#include "db.h"
#include "market.h"
#include "time.h"


using namespace std;
namespace my = mysqlpp;
namespace po = boost::program_options;


typedef struct {
	int time;
	double price, vol;
} trans_t;


po::options_description opt("Options");
int tick_len;
bool by_vol;
string direction;
int percentile;
int gap_threshold;
int from;
int ticker_count;
bool binary;

double total_vol = 0;
int gap_count = 0;
ofstream bout;
		

void parse_command_line(int argc, char** argv) {
	opt.add_options()
		("tick-len,l", po::value<int>(&tick_len)->default_value(300), "ticker length in seconds")
		("tick-vol,v", po::value<int>(&tick_len), "ticker volume in secondary currency")
		("direction,d", po::value<string>()->default_value(""), "export one direction only (bid/ask)")
		("percentile,p", po::value<int>(&percentile)->default_value(15), "high/low price percentile (0-50)")
		("gap-threshold,g", po::value<int>(&gap_threshold)->default_value(5), "minimum number of ticks/minutes without transactions to be considered as a gap")
		("from,f", po::value<string>(), "make candles from a given time")
		("ticker-count,c", po::value<int>(&ticker_count)->default_value(INT_MAX), "maximum ticker count to output")
		("binary,b", "binary output to file");
	
	if (argc < 3) throw po::error("");
	
	po::variables_map vm;
	po::store(po::parse_command_line(argc - 2, argv + 2, opt), vm);
	po::notify(vm);

	binary = vm.count("binary");
	by_vol = vm.count("tick-vol");
	direction = vm["direction"].as<string>();

	if (by_vol) gap_threshold *= 60;

	if (vm.count("from")) {
		from = parse_time(vm["from"].as<string>());
	} else {
		from = 0;
	}
}


template <typename T>
void bwrite(T& data) {
	bout.write((char*)&data, sizeof(T));
}


void write_ticker(vector<trans_t>& trans, bool by_vol, int next_time = 0) {
	quote q;
	q.open = trans.front().price;
	q.close = trans.back().price;
	q.avg = 0.0;
	q.vol = 0.0;
	for (trans_t& tx : trans) {
		q.vol += tx.vol;
	}
	
	int span = (next_time ? next_time : trans.back().time) - trans.front().time;
	sort(trans.begin(), trans.end(), [](const trans_t& a, const trans_t& b) { return a.price < b.price; });
	double cumVolume = 0;
	for (trans_t& tx : trans) {
		if (isnan(q.high) || tx.price > q.high) q.high = tx.price;
		if (isnan(q.low) || tx.price < q.low) q.low = tx.price;
		q.avg += tx.price * tx.vol;

		cumVolume += tx.vol;
		double perc = cumVolume / q.vol * 100;
		if (isnan(q.low_perc) && perc >= percentile) 
			q.low_perc = tx.price;
		if (isnan(q.high_perc) && perc + 1e-5 >= 100 - percentile) {
			q.high_perc = tx.price;
		}
	}
	q.avg /= q.vol;
	if (by_vol) {
		q.vol = 61 * q.vol / (span+1);
	}

	if (binary) {
		bwrite(q);
	} else {
		cout << q.open << ',' << q.high << ',' << q.low << ',' << q.close << ',' << q.high_perc << ',' << q.low_perc << ',' << q.avg << ',' << q.vol << endl;
	}

	ticker_count--;
}


void write_null_ticker(double price) {
	quote q;
	if (binary) {
		q.open = q.high = q.low = q.close = q.avg = price;
		q.vol = 0;
		bwrite(q);
	} else {
		for (int i = 0; i < 7; i++) {
			cout << price << ',';
		}
		cout << 0.0 << endl;
	}
	ticker_count--;
}


void write_gap(int len, int start, int tm) {
	if (binary) {
		gap g(len);
		bwrite(g);
	} else {
		cout << len << " ticks gap from " << format_time(start) << " to " << format_time(tm) << endl;
	}
	gap_count++;
}


void process_vol_ticker(my::Row& row, vector<trans_t>& trans)
{
	int tm = row[0];
	if (!trans.empty()) {
		int last_time = trans.back().time;
		if (tm - last_time > gap_threshold) {
			write_ticker(trans, true);
			write_gap(tm - last_time, last_time, tm);
			trans.clear();
			total_vol = 0.0;
		}
	}

	if (total_vol > tick_len) {
		write_ticker(trans, true, tm);
		trans.clear();
		total_vol = 0.0;
	}
	total_vol += (double)row[2];
}


void process_time_ticker(my::Row& row, vector<trans_t>& trans)
{
	if (trans.empty()) return;

	int tm = row[0];
	int start = trans.front().time / tick_len * tick_len;
	if (tm >= start + tick_len) {
		double last_price = trans.back().price;
		write_ticker(trans, false);
		trans.clear();

		int empty_tickers = (tm  - start) / tick_len - 1;
		if (empty_tickers >= gap_threshold) {
			write_gap((tm - start) / tick_len - 1, start, tm);
		} else if (empty_tickers > 0) {
			for (int i = 0; i < empty_tickers; i++) {
				write_null_ticker(last_price);
				if (ticker_count == 0) break;
			}
		}
	}
}


int main(int argc, char** argv) {
	try {
		parse_command_line(argc, argv);

		db db;
		db.execute("SET time_zone='UTC'");

		int exchange_id = db.get_exchange_id(argv[1]);
		int pair_id = db.get_pair_id(argv[2]);

		my::StoreQueryResult r_start = db.query("SELECT UNIX_TIMESTAMP(MIN(time)) FROM trans WHERE exchange_id = %0q AND pair_id = %1q AND time >= FROM_UNIXTIME(%2q)").store(exchange_id, pair_id, from);
		my::sql_int_unsigned_null f_start = r_start[0][0];
		if (f_start.is_null) {
			throw my::BadQuery(string("No transactions for ") + string(argv[2]));
		}
		int start = f_start.data / tick_len * tick_len;

		if (binary) {
			ios_base::sync_with_stdio(false);
			stringstream filename;
			filename << "ticker_" << argv[2] << "_" << (direction.empty() ? "" : direction + "_") << tick_len << (by_vol ? "v" : "s") << ".dat";
			bout.open(filename.str(), ios::binary | ios::out);
		}

		if (binary) {
			bwrite(start);
			bwrite(tick_len);
		} else {
			cout << fixed << setprecision(8);
			cout << start << "," << tick_len << endl;
		}
		
		if (direction == "bid") direction = "buy";
		else if (direction == "ask") direction = "sell";

		my::StoreQueryResult r_count;
		my::UseQueryResult r_trans;
		if (direction.empty()) {
			r_count = db.query("SELECT COUNT(*) FROM trans WHERE exchange_id = %0q AND pair_id = %1q AND time >= FROM_UNIXTIME(%2q)").store(exchange_id, pair_id, from);
			r_trans = db.query("SELECT UNIX_TIMESTAMP(time), price, vol FROM trans WHERE exchange_id = %0q AND pair_id = %1q AND time >= FROM_UNIXTIME(%2q) ORDER BY trans_id").use(exchange_id, pair_id, from);
		} else {
			r_count = db.query("SELECT COUNT(*) FROM trans WHERE exchange_id = %0q AND pair_id = %1q AND time >= FROM_UNIXTIME(%2q) AND dir = %3q").store(exchange_id, pair_id, from, direction);
			r_trans = db.query("SELECT UNIX_TIMESTAMP(time), price, vol FROM trans WHERE exchange_id = %0q AND pair_id = %1q AND time >= FROM_UNIXTIME(%2q) AND dir = %3q ORDER BY trans_id").use(exchange_id, pair_id, from, direction);
		}
		int txTotal = r_count[0][0];
		int txCount = 0;
		vector<trans_t> trans;

		while (my::Row row = r_trans.fetch_row()) {
			if (by_vol) {
				process_vol_ticker(row, trans);
			} else {
				process_time_ticker(row, trans);
			}
			if (ticker_count == 0) break;
		
			txCount++;
			if (binary && txCount % 200000 == 0) {
				cout << txCount << "/" << txTotal << " txs processed" << endl;
			}

			trans.push_back({ row[0], row[1], row[2] });
		}
		if (ticker_count != 0) {
			write_ticker(trans, by_vol);
		}
		cout << gap_count << " gaps occurred" << endl;

	} catch (po::error& e) {
		cerr << "Usage: " << argv[0] << " <exchange-name> <pair-code> [options]" << endl;
		cerr << opt;
		return 1;
	} catch (exception& e) {
		cerr << e.what() << endl;
		return 1;
	}
}


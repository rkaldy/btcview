#ifndef SAVITZKY_GOLAY_H
#define SAVITZKY_GOLAY_H

#include "filter.h"


class savitzky_golay : public filter {
public:
	/*
	 * Filter parameters
	 *
	 * deg			Filter degree
	 * w			Regression weight vector
	 * v, vplus		Precomputed Vandermonde matrix and its pseudoinverse.
	 * cp			Precomputed matrix with prediction coefficients
	 */
	int deg;
	VectorXd w;
	MatrixXd v, vplus;
	MatrixXd cp;

	/*
	 * Create Savitzky-Golay filter and precompute its Vandermonde matrices
	 *
	 * @in deg		Regression curve degree
	 * @in len		Filter length
	 * @in off		Absolute pivot offset (filter begin = pivot - off)
	 * @in pred_len	Number of points beyond filter end you want to predict. Zero if you don't want to predict anything.
	 */
	savitzky_golay(int deg, int len, int off, int pred_len = 0);
	
	/*
	 * Create weighted Savitzky-Golay filter and precompute its Vandermonde matrices.
	 * Weights are automatically adjusted to have sum 1
	 *
	 * @in deg		Regression curve degree
	 * @in len		Filter length
	 * @in off		Absolute pivot offset (filter begin = pivot - off)
	 * @in pred_len	Number of points beyond filter end you want to predict. Zero if you don't want to predict anything.
	 * @in w		Filter fitting weights (<length>-sized vector). Null for non-weighted filter.
	 */
	savitzky_golay(int deg, int len, int off, const VectorXd& w, int pred_len = 0);
	
	/*
	 * Create Savitzky-Golay filter with exponentially decaying weights.
	 *
	 * @in deg			Regression curve degree
	 * @in len_eff		Effective filter length (distance from pivot, where weight is one half)
	 * @in off_ratio	Filter offset as a ratio of filter length (0.5 <= off_ratio <= 1)
	 * @in pred_len		Number of points beyond filter end you want to predict. Zero if you don't want to predict anything.
	 */
	savitzky_golay(int deg, double len_eff, double off_ratio, int pred_len = 0);

	/**
	 * Compute residual sum of filter regression curve at a given point. 
	 * If the filter is weighted, the residual sum is weighted too.
	 *
	 * @in y		Input data
	 * @in x		Pivot index in the input data
	 *
	 * @return		Residual sum at the given pivot
	 */
	double residual_sum(const VectorXd& y, int x) const;

	/**
	 * Compute filter regression curve at a given point
	 *
	 * @in y		Input data
	 * @in x		Pivot index in the input data
	 *
	 * @return 		Regression curve (<len>-sized vector) around the pivot
	 */
	VectorXd regression_curve(const VectorXd& y, int x) const;

	/**
	 * Compute predicted values beyond source data based on filter smoothing
	 * Number of predicted values is dependent on particular filter type.
	 *
	 * @in y		Input data
	 * @in x		Pivot index in the input data
	 *
	 * @return		Vector of predicted values
	 */
	VectorXd predict(const VectorXd& y, int x) const;

protected:
	/**
	 * Precompute Vandermonde matrices, smoothing coefficients and prediction matrix
	 */
	void build(int pred_len);
};

#endif  // SAVITZKY_GOLAY_H

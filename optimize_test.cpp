#define EIGEN_DONT_ALIGN_STATICALLY
#include "test.h"
#include "optimize.h"


TEST(optimize, decode_point_test) {
	point<3> p;
	decode_point(5, {{ {1,2}, {3,4}, {5,6} }}, 2, p);
	EXPECT_EQ(p[0], 2);
	EXPECT_EQ(p[1], 3);
	EXPECT_EQ(p[2], 6);
}


double rosenbrock1(const Vector2d& x) {
	double d = 1 - x[0];
	double e = x[1] - x[0]*x[0];
	return d*d + 100*e*e;
}

TEST(optimize, simple) {
	vector<Vector2d> init(3);
	init[0] << 3,0;
	init[1] << -3,0;
	init[2] << 0,3;
	auto ret = optimize<Vector2d>(rosenbrock1, 1e-8, init);
	EXPECT_NEAR(ret.second, 0, 1e-6);
	EXPECT_NEAR(ret.first[0], 1, 0.01);
	EXPECT_NEAR(ret.first[1], 1, 0.01);
}


double rosenbrock2(const Vector2d& x, const int& a) {
	double d = a - x[0];
	double e = x[1] - x[0]*x[0];
	return d*d + 100*e*e;
}

TEST(optimize, arg) {
	vector<Vector2d> init(3);
	init[0] << 3,0;
	init[1] << -3,0;
	init[2] << 0,3;	
	auto ret = optimize<Vector2d, int>(rosenbrock2, 2, 1e-8, init);
	EXPECT_NEAR(ret.second, 0, 1e-6);
	EXPECT_NEAR(ret.first[0], 2, 0.01);
	EXPECT_NEAR(ret.first[1], 4, 0.01);
}


double rosenbrock3(const point<2>& x) {
	double d = 1 - x[0];
	double e = x[1] - x[0]*x[0];
	return d*d + 100*e*e;
}

TEST(optimize, grid) {
	array<bound_t, 2> bounds = {{{-20,20}, {-20,20}}};
	auto ret = optimize<2>(rosenbrock3, 1e-8, 10, bounds);
	EXPECT_NEAR(ret.second, 0, 1e-6);
	EXPECT_NEAR(ret.first[0], 1, 0.01);
	EXPECT_NEAR(ret.first[1], 1, 0.01);
}


double rosenbrock4(const point<2>& x, const int& a) {
	double d = a - x[0];
	double e = x[1] - x[0]*x[0];
	return d*d + 100*e*e;
}

TEST(optimize, grid_arg) {
	array<bound_t, 2> bounds = {{{-20,20}, {-20,20}}};
	auto ret = optimize<2, int>(rosenbrock4, 3, 1e-8, 11, bounds);
	EXPECT_NEAR(ret.second, 0, 1e-6);
	EXPECT_NEAR(ret.first[0], 3, 0.01);
	EXPECT_NEAR(ret.first[1], 9, 0.01);
}

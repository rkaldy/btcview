#include <cstdio>
#include <cstdlib>
#include <iostream>
#include "test.h"
#include "db.h"
#include "market.h"

using namespace mysqlpp;


const char* DATA_FILE = "ticker_USDT-BTC_300s.dat";
const int START_TIME = 1511992800;
const double price[] = { 1.3, 1.6, 1.0, 1.5, 1.9, 1.4, 2.0, 1.8, 1.1, 1.2, 1.7 };


class market_test: public testing::Test {
protected:
	db dbh;

	virtual void SetUp() {
		dbh.execute("SET time_zone='UTC'");
		dbh.execute("DELETE FROM trans WHERE exchange_id = 99");
		int pair1 = dbh.get_pair_id("USDT-BTC");
		int pair2 = dbh.get_pair_id("USDT-LTC");
		int pair3 = dbh.get_pair_id("USDT-ETH");
		Query ins = dbh.query("INSERT INTO trans (exchange_id, pair_id, trans_id, time, dir, price, vol) VALUES (99, %0q, %1q, FROM_UNIXTIME(%2q), %3q, %4q, %5q)");
		for (int t = 0; t < 5; t++) {
			for (int i = 0; i <= 10; i++) {
				unsigned int time;
				time = START_TIME + 300*t + i;
				string dir = i % 2 ? "buy" : "sell";
				ins.execute(pair1, t*11 + i, time + (t > 2 ? 910 : 0), dir, t + price[i], 1);
				ins.execute(pair2, t*11 + i, time, dir, t + price[i], 1);
				ins.execute(pair3, t*11 + i, START_TIME + t*11 + i + (t*11+i > 25 ? 300 : 0), dir, t + price[i], i+1);
			}
		}
	}

	virtual void TearDown() {
		remove(DATA_FILE);
	}
};


TEST_F(market_test, parser) {
	system("./candle test USDT-LTC -b -l 300 > /dev/null");
	market m("ticker_USDT-LTC_300s.dat");

	EXPECT_EQ(m.start, START_TIME);
	EXPECT_EQ(m.step, 300);
	ASSERT_EQ(m.size(), 1);
	ASSERT_EQ(m.chunks[0].quotes.size(), 5);
	EXPECT_EQ(m.chunks[0].gap, 0);
	for (int t = 0; t < 5; t++) {
		EXPECT_DOUBLE_EQ(m.chunks[0].quotes[t].open,  t + 1.3);
		EXPECT_DOUBLE_EQ(m.chunks[0].quotes[t].high,  t + 2.0);
		EXPECT_DOUBLE_EQ(m.chunks[0].quotes[t].low,   t + 1.0);
		EXPECT_DOUBLE_EQ(m.chunks[0].quotes[t].close, t + 1.7);
		EXPECT_DOUBLE_EQ(m.chunks[0].quotes[t].avg,   t + 1.5);
		EXPECT_DOUBLE_EQ(m.chunks[0].quotes[t].vol,   11);
	}
	remove("ticker_USDT-LTC_300s.dat");
}


TEST_F(market_test, gap) {
	system("./candle test USDT-BTC -b -l 300 -g 3 > /dev/null");
	market m(DATA_FILE);

	ASSERT_EQ(m.size(), 2);
	ASSERT_EQ(m.chunks[0].quotes.size(), 3);
	EXPECT_EQ(m.chunks[0].gap, 3);
	ASSERT_EQ(m.chunks[1].quotes.size(), 2);
	for (int t = 0; t <= 2; t++) {
		EXPECT_DOUBLE_EQ(m.chunks[0].quotes[t].open,  t + 1.3);
		EXPECT_DOUBLE_EQ(m.chunks[0].quotes[t].high,  t + 2.0);
		EXPECT_DOUBLE_EQ(m.chunks[0].quotes[t].low,   t + 1.0);
		EXPECT_DOUBLE_EQ(m.chunks[0].quotes[t].close, t + 1.7);
		EXPECT_DOUBLE_EQ(m.chunks[0].quotes[t].avg,   t + 1.5);
		EXPECT_DOUBLE_EQ(m.chunks[0].quotes[t].vol,   11);
	}
	for (int t = 0; t <= 1; t++) {
		EXPECT_DOUBLE_EQ(m.chunks[1].quotes[t].open,  t + 3 + 1.3);
		EXPECT_DOUBLE_EQ(m.chunks[1].quotes[t].high,  t + 3 + 2.0);
		EXPECT_DOUBLE_EQ(m.chunks[1].quotes[t].low,   t + 3 + 1.0);
		EXPECT_DOUBLE_EQ(m.chunks[1].quotes[t].close, t + 3 + 1.7);
		EXPECT_DOUBLE_EQ(m.chunks[1].quotes[t].avg,   t + 3 + 1.5);
		EXPECT_DOUBLE_EQ(m.chunks[1].quotes[t].vol,   11);
	}
}


TEST_F(market_test, ticker_count) {
	system("./candle test USDT-BTC -b -l 300 -g 3 > /dev/null");
	market m(DATA_FILE);

	ASSERT_EQ(m.size(), 2);
	EXPECT_EQ(m.ticker_count(false), 5);
	EXPECT_EQ(m.ticker_count(true), 8);
}


TEST_F(market_test, extract) {
	VectorXd open_exp(3);	open_exp << 1.3, 2.3, 3.3;
	VectorXd avg_exp(2);	avg_exp  << 4.5, 5.5;

	system("./candle test USDT-BTC -b -l 300 -g 3 > /dev/null");
	market m(DATA_FILE);

	ASSERT_EQ(m.size(), 2);
	EXPECT_VECTORS_NEAR(m.extract(0, &quote::open), open_exp);
	EXPECT_VECTORS_NEAR(m.extract(1, &quote::avg), avg_exp);
}


TEST_F(market_test, extract_with_gaps) {
	double open_exp[]  = { 1.3, 2.3, 3.3, NAN, NAN, NAN, 4.3, 5.3 };
	double avg_exp[] =   { 1.5, 2.5, 3.5, NAN, NAN, NAN, 4.5, 5.5 };

	system("./candle test USDT-BTC -b -l 300 -g 3 > /dev/null");
	market m(DATA_FILE);

	vector<double> open = m.extract(&quote::open);
	vector<double> avg  = m.avg();
	ASSERT_EQ(open.size(), 8);
	ASSERT_EQ(avg.size(), 8);

	for (int i = 0; i < 8; i++) {
		if (i >= 3 && i <= 5) {
			EXPECT_TRUE(isnan(open[i]));
			EXPECT_TRUE(isnan(avg[i]));
		} else {
			EXPECT_DOUBLE_EQ(open[i], open_exp[i]);
			EXPECT_DOUBLE_EQ(avg[i], avg_exp[i]);
		}
	}
}


TEST_F(market_test, ticker) {
	double open_exp[]  = { 1.3, 2.3, 3.3, NAN, NAN, NAN, 4.3, 5.3 };
	double close_exp[] = { 1.7, 2.7, 3.7, NAN, NAN, NAN, 4.7, 5.7 };

	system("./candle test USDT-BTC -b -l 300 -g 3 > /dev/null");
	market m(DATA_FILE);

	vector<ticker_t> ticker = m.ticker();
	ASSERT_EQ(ticker.size(), 8);
	for (int i = 0; i < 8; i++) {
		if (i >= 3 && i <= 5) {
			EXPECT_TRUE(isnan(ticker[i].open));
			EXPECT_TRUE(isnan(ticker[i].close));
		} else {
			EXPECT_DOUBLE_EQ(ticker[i].open, open_exp[i]);
			EXPECT_DOUBLE_EQ(ticker[i].close, close_exp[i]);
		}
	}

	ticker = m.ticker(0);
	ASSERT_EQ(ticker.size(), 3);
	for (int i = 0; i < 3; i++) {
		EXPECT_DOUBLE_EQ(ticker[i].open,  open_exp[i]);
		EXPECT_DOUBLE_EQ(ticker[i].close, close_exp[i]);
	}
}


TEST_F(market_test, gap_threshold) {
	VectorXd avg_exp(8);	avg_exp << 1.5, 2.5, 3.5, 3.7, 3.7, 3.7, 4.5, 5.5;
	VectorXd vol_exp(8);	vol_exp << 11, 11, 11, 0, 0, 0, 11, 11;

	system("./candle test USDT-BTC -b -l 300 > /dev/null");
	market m(DATA_FILE);

	ASSERT_EQ(m.size(), 1);
	EXPECT_VECTORS_NEAR(m.extract(0, &quote::avg), avg_exp);
}


TEST_F(market_test, logarithmic) {
	VectorXd close_exp(3);	close_exp << log(1.7), log(2.7), log(3.7);
	VectorXd vol_exp(3);	vol_exp << 11, 11, 11;
	
	system("./candle test USDT-BTC -b -l 300 -g 3 > /dev/null");
	market m(DATA_FILE, true);

	ASSERT_EQ(m.size(), 2);
	EXPECT_VECTORS_NEAR(m.extract(0, &quote::close), close_exp);
	EXPECT_VECTORS_NEAR(m.extract(0, &quote::vol), vol_exp);
}


TEST_F(market_test, percentile) {
	VectorXd high_perc_exp(3);	high_perc_exp << 1.9, 2.9, 3.9;
	VectorXd low_perc_exp(3);	low_perc_exp  << 1.1, 2.1, 3.1;
	
	system("./candle test USDT-BTC -b -l 300 -g 3 -p 15 > /dev/null");
	market m(DATA_FILE);

	ASSERT_EQ(m.size(), 2);
	EXPECT_VECTORS_NEAR(m.extract(0, &quote::high_perc), high_perc_exp);
	EXPECT_VECTORS_NEAR(m.extract(0, &quote::low_perc), low_perc_exp);
}


TEST_F(market_test, volume_ticker) {
	VectorXd open_exp_0(3);		open_exp_0 << 1.3, 2.3, 3.3;
	VectorXd open_exp_1(3);		open_exp_1 << 3.9, 4.9, 5.9;
	VectorXd close_exp_0(3);	close_exp_0 << 1.7, 2.7, 3.5;
	VectorXd close_exp_1(3);	close_exp_1 << 4.5, 5.5, 5.7;
	VectorXd vol_exp_0(3);		vol_exp_0  << 66.0*61/12, 66.0*61/12, 10.0*61/4;
	VectorXd vol_exp_1(3);		vol_exp_1  << 66.0*61/12, 66.0*61/12, 56.0*61/7;
		
	system("./candle test USDT-ETH -b -v 62 -g 5 > /dev/null");
	market m("ticker_USDT-ETH_62v.dat");

	ASSERT_EQ(m.size(), 2);
	EXPECT_VECTORS_NEAR(m.extract(0, &quote::open), open_exp_0);
	EXPECT_VECTORS_NEAR(m.extract(0, &quote::close), close_exp_0);
	EXPECT_VECTORS_NEAR(m.extract(0, &quote::vol), vol_exp_0);
	EXPECT_VECTORS_NEAR(m.extract(1, &quote::open), open_exp_1);
	EXPECT_VECTORS_NEAR(m.extract(1, &quote::close), close_exp_1);
	EXPECT_VECTORS_NEAR(m.extract(1, &quote::vol), vol_exp_1);

	remove("ticker_USDT-ETH_62v.dat");
}


TEST_F(market_test, candle_from) {
	VectorXd avg_exp(2);	avg_exp  << 4.5, 5.5;

	system("./candle test USDT-BTC -b -l 300 -f '29.11.2017 22:30:00' > /dev/null");
	market m(DATA_FILE);

	ASSERT_EQ(m.size(), 1);
	EXPECT_VECTORS_NEAR(m.extract(0, &quote::avg), avg_exp);
}


TEST_F(market_test, candle_ticker_count) {
	VectorXd avg_exp(4);	avg_exp << 1.5, 2.5, 3.5, 3.7;

	system("./candle test USDT-BTC -b -l 300 -c 4 > /dev/null");
	market m(DATA_FILE);

	ASSERT_EQ(m.size(), 1);
	EXPECT_VECTORS_NEAR(m.extract(0, &quote::avg), avg_exp);
}


TEST_F(market_test, direction) {
	VectorXd open_exp(5);	open_exp << 1.6, 2.6, 3.6, 4.6, 5.6;
	VectorXd close_exp(5);	close_exp << 1.2, 2.2, 3.2, 4.2, 5.2;

	system("./candle test USDT-LTC -b -l 300 -d bid  > /dev/null");
	market m("ticker_USDT-LTC_bid_300s.dat");

	EXPECT_VECTORS_NEAR(m.extract(0, &quote::open), open_exp);
	EXPECT_VECTORS_NEAR(m.extract(0, &quote::close), close_exp);
	
	remove("ticker_USDT-LTC_bid_300s.dat");
}

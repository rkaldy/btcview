#include <array>
#include <vector>
#include <map>
#include <string>
#include <iostream>
#include <algorithm>
#include <functional>
#include <utility>
#include "common.h"

using namespace std;


/*
 * Nelder-Mead optimization. Does not require cost function derivatives.
 *
 * @template Point	State space point type. Must implement addition, subtraction and real number multiplication.
 *
 * @in cost			Cost function (Point->double)
 * @in threshold	When two best simplex points have cost difference smaller than the threshold, finish the optimization
 * @in init			Initial simplex values. Vector of (n+1) linear independent points, where (n) is the state space dimension.
 *
 * @return			Optimal point and its cost
 */
template<typename Point>
pair<Point, double> optimize(function<double(const Point&)> cost, double threshold, const vector<Point>& init) {
	
	class Entry {
	public:
		Point pt;
		double val;
		Entry() {}
		Entry(const Point& p, function<double(const Point&)> cost) : pt(p), val(cost(p)) {}
		bool operator<(const Entry& a) { return val < a.val; }
	};

	typedef typename vector<Entry>::iterator EntryIt;

	int i = 0, n = init.size();
	vector<Entry> x(n);
	for (auto p : init) x[i++] = Entry(p, cost);
	sort(x.begin(), x.end());
	
	Point s = x[0].pt;	// there's no general way how to initialize Point to zero
	for (i = 1; i < n-1; i++) s += x[i].pt;
	s *= 1./(n-1);

	while (true) {
		Entry r(s * 2 - x[n-1].pt, cost);
		if (r < x[0]) {	
			Entry e(r.pt * 2 - s, cost);
			if (e < r) {
				TRACE("expansion " << e.pt << " cost=" << e.val);
				r = e;
			} else {
				TRACE("reflection " << r.pt << " cost=" << r.val);
			}
			for (i = n-2; i >= 0; i--) x[i+1] = x[i];
			x[0] = r;
			s += (r.pt - x[n-1].pt) * (1./(n-1));
		}
		else if (r < x[n-2]) {
			TRACE("reflection " << r.pt << " cost=" << r.val);
			EntryIt xi = lower_bound(x.begin(), x.end(), r);
			for (i = n-2; i >= xi - x.begin(); i--) x[i+1] = x[i];
			*xi = r;
			s += (r.pt - x[n-1].pt) * (1./(n-1));
		}
		else {
			r = Entry((s + x[n-1].pt) * 0.5, cost);
			if (r < x[n-1]) {
				TRACE("contraction " << r.pt << " cost=" << r.val);
				EntryIt xi = lower_bound(x.begin(), x.end(), r);
				for (i = n-2; i >= xi - x.begin(); i--) x[i+1] = x[i];
				*xi = r;
				s += (r.pt - x[n-1].pt) * (1./(n-1));
			} else {
				TRACE("shrink");
				TRACE("  " << x[0].pt << " cost=" << x[0].val);
				for (i = 1; i < n; i++) {
					x[i] = Entry((x[0].pt + x[i].pt) * 0.5, cost);
					TRACE("  " << x[i].pt << " cost=" << x[i].val);
				}
				sort(x.begin(), x.end());
				s *= 0;
				for (i = 0; i < n-1; i++) s += x[i].pt;
				s *= 1./(n-1);
			}
			if (x[1].val - x[0].val < threshold) {
				return make_pair(x[0].pt, x[0].val);
			}
		}
	}
}


/*
 * Nelder-Mead optimization. Does not require cost function derivatives.
 *
 * @template Point	State space point. Must implement addition, subtraction and scalar multiplication.
 * @template Arg	Additional cost function argument, can be an arbitrary type
 *
 * @in cost			Cost function (Point,Arg->double)
 * @in arg			Additional cost function argument. It's constant during optimization.
 * @in threshold	When two best simplex points have cost difference smaller than the threshold, finish the optimization
 * @in init			Initial simplex values. Vector of (n+1) non-coplanar points, where _n_ is a state space dimension.
 *
 * @return			Optimal point and its cost
 */
template<typename Point, typename Arg>
pair<Point, double> optimize(function<double(const Point&, const Arg& arg)> cost, const Arg& arg, double threshold, vector<Point> init) {
	return optimize<Point>(bind(cost, placeholders::_1, arg), threshold, init);
}


template <size_t N>
class point : public array<double, N> {
public:
	using array<double, N>::array;

	point<N> operator+(const point<N>& p) {
		point<N> ret;
		for (size_t i = 0; i < N; i++) ret[i] = this->at(i) + p[i];
		return ret;
	}
	
	point<N> operator-(const point<N>& p) {
		point<N> ret;
		for (size_t i = 0; i < N; i++) ret[i] = this->at(i) - p[i];
		return ret;
	}

	point<N> operator*(double x) {
		point<N> ret;
		for (size_t i = 0; i < N; i++) ret[i] = this->at(i) * x;
		return ret;
	}

	point<N>& operator*=(double x) {
		for (size_t i = 0; i < N; i++) this->at(i) *= x;
		return *this;
	}

	point<N>& operator+=(const point<N>& p) {
		for (size_t i = 0; i < N; i++) this->at(i) += p[i];
		return *this;
	}

	point<N>& operator-=(const point<N>& p) {
		for (size_t i = 0; i < N; i++) this->at(i) -= p[i];
		return *this;
	}

	friend ostream& operator<<(ostream& os, const point<N>& p) {
		os << "[ ";
		for (size_t i = 0; i < N; i++) os << p[i] << " ";
		os << "]";
		return os;
	}
};


typedef struct {
	double from, to;
} bound_t;


template <size_t N>
void decode_point(int q, const array<bound_t, N>& bounds, int gran, point<N>& p) {
	for (size_t d = 0; d < N; d++) {
		p[d] = bounds[d].from + (bounds[d].to - bounds[d].from) * (q % gran) / (gran - 1);
		q /= gran;
	}
}


/*
 * Find optimum in a N-dimensional state space.
 * Create gran^N lattice points, take the best N+1, and use these as initial simplex for Nelder-Mead optimization.
 *
 * @template N		Space dimension
 *
 * @in cost			Cost function (Point->double)
 * @in threshold	When two best simplex points have cost difference smaller than the threshold, finish the optimization
 * @in gran			Number of lattice points in one dimension (>=2)
 * @in bounds		N-sized array of lower and upper bound for each dimension.
 *
 * @return			Optimal point and its cost
 */
template <size_t N>
pair<point<N>, double> optimize(function<double(const point<N>&)> cost, double threshold, int gran, array<bound_t, N> bounds) {
	assert(gran >= 2);

	int s = 1;
	point<N> p;
	for (size_t d = 0; d < N; d++) s *= gran;
	double minc = INT_MAX, minq;

	for (int q = 0; q < s; q++) {
		decode_point(q, bounds, gran, p);
		double c = cost(p);
		TRACE("  " << p << " cost=" << c);
		if (c < minc) {
			minc = c;
			minq = q;
		}
	}

	vector<point<N>> init(N + 1);
	decode_point(minq, bounds, gran, init[0]);
	TRACE("init");
	TRACE("  " << init[0] << " cost=" << minc);
	for (size_t d = 0; d < N; d++) {
		point<N> n1 = init[0], n2 = init[0];
		n1[d] += (bounds[d].to - bounds[d].from) / (gran - 1);
		n2[d] -= (bounds[d].to - bounds[d].from) / (gran - 1);
		double c1 = cost(n1);
		double c2 = cost(n2);
		if (c1 < c2) {
			init[d+1] = n1;
		} else {
			init[d+1] = n2;
		}
		TRACE("  " << init[d+1] << " cost=" << min(c1, c2));
	}

	return optimize(cost, threshold, init);
}


/*
 * Find optimum in a N-dimensional state space.
 * Create gran^N lattice points, take the best N+1, and use these as initial simplex for Nelder-Mead optimization.
 *
 * @template N		Space dimension
 * @template Arg	Additional cost function argument, can be an arbitrary type
 *
 * @in cost			Cost function (Point,Arg->double)
 * @in arg			Additional cost function argument. It's constant during optimization.
 * @in threshold	When two best simplex points have cost difference smaller than the threshold, finish the optimization
 * @in gran			Number of lattice points in one dimension (>=2)
 * @in bounds		N-sized array of lower and upper bound for each dimension.
 *
 * @return			Optimal point and its cost
 */
template <size_t N, typename Arg>
pair<point<N>, double> optimize(function<double(const point<N>&, const Arg& arg)> cost, const Arg& arg, double threshold, int gran, array<bound_t, N> bounds) {
	return optimize<N>(bind(cost, placeholders::_1, arg), threshold, gran, bounds);
}

#ifndef TEST_H
#define TEST_H

#include <Eigen/Dense>
#include <gtest/gtest.h>

using namespace std;
using namespace Eigen;


const double DblPrecision = 1e-6;


template <typename Vector>
string str_vector(Vector const& v) {
	stringstream ss;
	ss << "[ ";
	for (int i = 0; i < (int)v.size(); i++) {
		ss << v[i] << ' ';
	}
	ss << ']';
	return ss.str();
}


template <typename Vector1, typename Vector2>
testing::AssertionResult compare_vectors(const char* u_expr, const char* v_expr, Vector1 const& u, Vector2 const& v) {
	if (u.size() == v.size()) {
		int i;
		for (i = 0; i < (int)u.size(); i++) {
			if ((std::isnan(u[i]) ^ std::isnan(v[i])) || std::abs(u[i] - v[i]) > DblPrecision) {
				break;
			}
		}
		if (i == (int)u.size()) {
			return testing::AssertionSuccess();
		}
	}
	return testing::AssertionFailure() << '\t' << u_expr << endl << "\tExpected: " << str_vector(v) << endl << "\tActual:   " << str_vector(u);
}


#define EXPECT_VECTORS_NEAR(u, v) EXPECT_PRED_FORMAT2(compare_vectors, u, v)
#define ASSERT_VECTORS_NEAR(u, v) ASSERT_PRED_FORMAT2(compare_vectors, u, v)


#endif  // TEST_H

#include "db.h"

using namespace mysqlpp;


const char* db::BUILTIN_QUERY_SQL[] = {
	"SELECT exchange_id FROM exchange WHERE name = %0q",
	"SELECT pair_id FROM pair WHERE code = %0q",
};


db::db() : conn(true) {
	conn.connect("btc2", "localhost", "user", "password");
	for (int i = 0; i < Q::size; i++) {
		builtin_query.push_back(conn.query(BUILTIN_QUERY_SQL[i]));
		builtin_query.back().parse();
	}
}


Query db::query(const char* sql) {
	Query q = conn.query(sql);
	q.parse();
	return q;
}


SimpleResult db::execute(const char* sql) {
	Query q = conn.query(sql);
	return q.execute();
}


int db::get_exchange_id(const char* name) {
	StoreQueryResult res = builtin_query[Q::Exchange].store(name);
	if (res.empty()) {
		throw BadQuery(string("Unknown exchange ") + string(name));
	}
	return res[0][0];
}


int db::get_pair_id(const char* name) {
	StoreQueryResult res = builtin_query[Q::Pair].store(name);
	if (res.empty()) {
		throw BadQuery(string("Unknown pair ") + string(name));
	}
	return res[0][0];
}

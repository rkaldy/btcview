#include <cmath>
#include <iostream>
#include <functional>
#include <parallel.h>
#include "stats.h"
#include "smoother.h"

using namespace std;


smoother::smoother(const market& market, int filter_count, int filter_deg, double filter_min_len, double filter_multiplier) {
	for (int ch = 0; ch < market.size(); ch++) {
		avg.push_back(market.avg(ch));
		spread.push_back(market.extract(ch, &quote::high_perc) -  market.extract(ch, &quote::low_perc));
	}
	for (int f = 0; f < filter_count; f++) {
		filters.emplace_back(filter_deg, filter_min_len * pow(filter_multiplier, f), 0.5);
	}
}


vector<VectorXd> smoother::smooth(size_t chunk) const {
	assert(chunk < avg.size());
	cout << "Smoothing" << endl;

	vector<VectorXd> sm;
	sm.resize(filters.size());
	parallel::cycle(0, 0, filters.size(), [this, &sm, chunk](int i){
		cout << "Filter #" << i << endl;
		sm[i] = filters[i].smooth(avg[chunk]);
	});
	return sm;
}


vector<double> smoother::residue(size_t chunk, int x, bool band) const {
	assert(chunk < avg.size());
#ifndef NDEBUG
	int xmin, xmax;
	pivot_limits(chunk, xmin, xmax);
	assert(x >= xmin);
	assert(x <= xmax);
#endif

	vector<double> res;
	for (int i = 0; i < filter_count(); i++) {
		if (band) {
			res.push_back(band_residual_sum(chunk, x, i));
		} else {
			res.push_back(filters[i].residual_sum(avg[chunk], x));
		}
	}
	return res;
}


vector<vector<double>> smoother::residue(size_t chunk, bool band) const {
	assert(chunk < avg.size());
	cout << "Computing residual sums" << endl;

	vector<vector<double>> resid(avg.size());
	for (vector<double>& r : resid) {
		r.resize(filters.size(), NAN);
	}
	
	int xmin, xmax;
	pivot_limits(chunk, xmin, xmax);

	parallel::cycle(0, 0, filters.size(), [this, &resid, chunk, band, xmin, xmax](int i){
		cout << "Filter #" << i << endl;
		for (int x = xmin; x <= xmax; x++) {
			if (band) {
				resid[x][i] = band_residual_sum(chunk, x, i);
			} else {
				resid[x][i] = filters[i].residual_sum(avg[chunk], x);
			}
		}
	});
	return resid;
}


vector<VectorXd> smoother::regression_curve(size_t chunk, int x) const {
	assert(chunk < avg.size());
#ifndef NDEBUG
	int xmin, xmax;
	pivot_limits(chunk, xmin, xmax);
	assert(x >= xmin);
	assert(x <= xmax);
#endif

	vector<VectorXd> rc(filters.size());
	for (size_t i = 0; i < filters.size(); i++) {
		const savitzky_golay& f = filters[i];
		rc[i] = VectorXd::Constant(avg[chunk].size(), NAN);
		rc[i].segment(x - f.off, f.len) = f.regression_curve(avg[chunk], x);
	}
	return rc;
}


void smoother::smooth_adaptive(size_t chunk, double threshold, vector<double>& y, vector<double>& filter_len) const {
	assert(chunk < avg.size());
	assert(threshold > 0);
	cout << "Adaptive smoothing" << endl;

	y = vector<double>(avg[chunk].size(), NAN);
	filter_len = vector<double>(avg[chunk].size(), NAN);
	int xmin, xmax;
	pivot_limits(chunk, xmin, xmax);

	parallel::cycle(0, xmin, xmax+1, [this, &y, &filter_len, chunk, threshold](int x) {
		size_t f;
		for (f = 0; f < filters.size(); f++) {
			if (band_residual_sum(chunk, x, f) > threshold) {
				break;
			}
		}
		if (f > 0) f--;
		filter_len[x] = binary_search(0, (int)filters.size(), threshold, bind(&smoother::band_residual_sum, this, chunk, x, placeholders::_1));
		y[x] = filters[filter_len[x]].smooth_point(avg[chunk], x);
		filter_len[x] = f;
		y[x] = filters[f].smooth_point(avg[chunk], x);
	});
}


void smoother::pivot_limits(size_t chunk, int& min, int& max) const {
	min = filters.back().off;
	max = avg[chunk].size() - 1 - filters.back().len + filters.back().off;
}


double smoother::band_residual_sum(size_t chunk, int x, size_t filter_id) const {
	const savitzky_golay& f = filters[filter_id];
	return f.residual_sum(avg[chunk], x) / f.w.dot(spread[chunk].segment(x - f.off, f.len));
}




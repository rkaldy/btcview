#include <cmath>
#include <cassert>
#include <Eigen/LU> 
#include "savitzky_golay.h"


const double WEIGHT_THRESHOLD = 0.05;
const double EFF_LEN_WEIGHT	  = 0.5;


savitzky_golay::savitzky_golay(int adeg, int len, int off, const VectorXd& aw, int pred_len) : filter(len, off), deg(adeg), w(aw) {
	assert(deg >= 1);
	assert(len >= deg);
	
	build(pred_len);
}


savitzky_golay::savitzky_golay(int deg, int len, int off, int pred_len) : savitzky_golay(deg, len, off, VectorXd::Ones(len), pred_len) 
{}


savitzky_golay::savitzky_golay(int adeg, double len_eff, double off_factor, int pred_len) : filter(1, 0), deg(adeg) {
	assert(deg >= 1);
	assert(len_eff > 0);
	assert(off_factor > 0 && off_factor <= 1);

	bool reverse = off_factor > 0.5;
	if (reverse) off_factor = 1 - off_factor;

	double q = -log(EFF_LEN_WEIGHT) / len_eff;
	double nl = len_eff * log(WEIGHT_THRESHOLD) / log(EFF_LEN_WEIGHT);
	off = lrint(ceil(nl * off_factor / (1 - off_factor)));
	len = lrint(ceil(nl)) + off + 1;
	if (reverse) off = len - 1 - off;

	w.resize(len);
	for (int i = 0; i < len; i++) {
		w(i) = exp(-q * abs(i - len + 1 + off));
	}
	build(pred_len);
}


double savitzky_golay::residual_sum(const VectorXd& y, int x) const {
	assert(x >= off);
	assert(x-off+len <= y.size());

	VectorXd ys = y.segment(x-off, len);
	VectorXd yhat = v * vplus * ys;
	ArrayXd e = (ys - yhat).array();
	return (e * e * w.array()).sum();
}	


VectorXd savitzky_golay::regression_curve(const VectorXd& y, int x) const {
	assert(x >= off);
	assert(x-off+len <= y.size());

	return v * vplus * y.segment(x-off, len);
}


VectorXd savitzky_golay::predict(const VectorXd& y, int x) const {
	assert(x >= off);
	assert(x-off+len <= y.size());

	return cp * y.segment(x-off, len);
}


void savitzky_golay::build(int pred_len) {
	assert(pred_len >= 0);

	v.resize(len, deg+1);
	vplus.resize(deg+1, len);
	w /= w.sum();

	// V := [i^j]_{ij}  i=0..deg, j=-s..x-s-1
	for (int i = 0; i < len; i++) {
		for (int j = 0; j <= deg; j++) {
			v(i, j) = pow(i - off, j);
		}
	}
		
	// precompute \sum_{k=0}^x w_k*k^i, i=0..2*deg
	double els[deg * 2 + 1];
	for (int i = 0; i <= deg * 2; i++) {
		els[i] = 0;
		for (int k = 0; k < len; k++) {
			els[i] += w(k) * pow(k - off, i);
		}
	}

	// R := X^t * W * X
	// R_ij = \sum_{k=0}^{x-1} w_k*k^{i+j}
	MatrixXd r(deg+1, deg+1);
	for (int i = 0; i <= deg; i++) {
		for (int j = 0; j <= deg; j++) {
			r(i, j) = els[i+j];
		}
	}

	// V^+ := R^{-1} * V^t * W
	vplus = r.inverse() * v.transpose();
	for (int i = 0; i < len; i++) {
		vplus.col(i) *= w(i);
	}

	// c = first row of V^+
	coef = vplus.row(0);

	if (pred_len) {
		MatrixXd vp(pred_len, deg+1);
		for (int i = 0; i < pred_len; i++) {
			for (int j = 0; j <= deg; j++) {
				vp(i, j) = pow(len + i - off, j);
			}
		}
		// C_p := V_p * V^+
		cp = vp * vplus;
	}
}

#include <vector>
#include <Eigen/Dense>
#include "market.h"
#include "savitzky_golay.h"

using namespace std;

/**
 * Smooth a quote series by a set of Savitzky-Golay filters with exponentially growing length.
 * It uses an average price for every ticker.
 */
class smoother {
public:

	/*
	 * Create a smoother with predefine filter set.
	 *
	 * @in market				Market data
	 * @in filter_count			Filter count
	 * @in filter_deg			Filter regression curve degree
	 * @in filter_min_len		Filter effective length for the shortest filter
	 * @in filter_multiplier	Filter effective length multiplier for subsequent filters
	 */
	smoother(const market& market, int filter_count, int filter_deg, double filter_min_len, double filter_multiplier);
	
	/**
	 * Smooth data
	 *
	 * @in chunk		Market data chunk
	 * @return 			Vector of smoothed data for each filter
	 */
	vector<VectorXd> smooth(size_t chunk) const;

	/**
	 * Compute residual sum at a given pivot
	 *
	 * @in chunk		Market data chunk
	 * @in x			Pivot index in the given chunk
	 * @in band			If true the residual sum is weighted proportionally to the
	 *  				spread between perc_high and perc_low at each point
	 * @return			Vector of residual sums for each filter
	 */
	vector<double> residue(size_t chunk, int x, bool band = false) const;

	/**
	 * Compute residual sum for every pivot
	 *
	 * @in chunk		Market data chunk
	 * @in band			If true the residual sum is weighted proportionally to the
	 *  				spread between perc_high and perc_low at each point
	 * @return			Vector of residual sums for each pivot and filter
	 */
	vector<vector<double>> residue(size_t chunk, bool band = false) const;
	
	/**
	 * Compute residual sum at a given point for a given filter.
	 * The sum is divided by sum of spreads between perc_high and perc_low at each point among filter length.
	 *
	 * @in chunk		Market data chunk
	 * @in x			Pivot index in the given chunk
	 * @in filter_d		Filter index in the current filterset
	 *
	 * @double			Residual sum
	 */
	double band_residual_sum(size_t chunk, int x, size_t filter_id) const;

	/**
	 * Compute filter regression curve for each filter at a given pivot.
	 *
	 * @in chunk		Market data chunk
	 * @in x			Pivot index in the given chunk
	 * @return			Vector of regression curves for each filter.
	 * 					The curve vectors have same length as the whole chunk, 
	 * 					they contain NANs at points outside their filter length
	 */
	vector<VectorXd> regression_curve(size_t chunk, int x) const;

	/**
	 * Adaptive data smoothing. For every pivot gives the longest filter 
	 * its proportional residual sum is less than <threshold>
	 *
	 * @in chunk		Market data chunk
	 * @in threshold	Residual sum threshold
	 * @out y			Smoothed data (avg[chunk].size()-sized vector)
	 * @out filter_len	Used filter length for each point (avg[chunk].size()-sized vector)
	 * 					The vector contains doubles even if its values are integral, to be
	 * 					compatible with graph class.
	 */
	void smooth_adaptive(size_t chunk, double threshold, vector<double>& y, vector<double>& filter_len) const;

	/**
	 * Get filterset size
	 */
	int filter_count() const 	{ return filters.size(); }

	/**
	 * Returns an interval where where all filters can be applied.
	 *
	 * @in chunk		Market data chunk
	 * @out min			Minimal valid pivot index
	 * @out max			Maximal valid pivot index
	 */
	void pivot_limits(size_t chunk, int& min, int& max) const;

protected:
	vector<VectorXd> avg, spread;
	vector<savitzky_golay> filters;
};

#ifndef FILTER_H
#define FILTER_H

#include <vector>
#include <Eigen/Dense>

using namespace std;
using namespace Eigen;


class filter {

public:
	/*
	 * Filter parameters
	 *
	 * len			Filter length
	 * off			Absolute pivot offset (filter begin = pivot - off)
	 * coef			Filter coefficients
	 */
	int len;
	int off;
	VectorXd coef;

	/**
	 * Create a new filter
	 *
	 * @in len		Filter absolute length
	 * @in off		Filter offset (pivot - filter begin)
	 */
	filter(int len, int off);

	/**
	 * Compute smoothed value at a given point
	 *
	 * @in src		Input data
	 * @in x		Pivot index in the input data
	 *
	 * @return		Smoothed value at pivot
	 */
	virtual double smooth_point(const VectorXd& src, int x) const;

	/**
	 * Smooth data series
	 *
	 * @in src		Input data
	 *
	 * @return		Smoothed data
	 */
	virtual VectorXd smooth(const VectorXd& src) const;

};


#endif  // FILTER_H

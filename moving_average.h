#ifndef MOVING_AVERAGE_H
#define MOVING_AVERAGE_H

#include "savitzky_golay.h"


class moving_average : public savitzky_golay {
public:
	/*
	 * Create simple moving average filter.
	 *
	 * @in len			Filter length
	 * @in off			Absolute pivot offset (filter begin = pivot - off)
	 */
	moving_average(int len, int off);
	
	/*
	 * Create exponential moving average filter.
	 *
	 * @in len_eff		Effective filter length (distance from pivot, where weight is one half)
	 * @in off_ratio	Filter offset as a ratio of filter length (0.5 <= off_ratio <= 1)
	 */
	moving_average(double len_eff, double off_ratio);
	
	/*
	 * Smooth data series using simple moving average. 
	 * Faster implementation than creating a filter
	 *
	 * @in src			Input data
	 * @in len			Filter length
	 * @in off			Absolute pivot offset (filter begin = pivot - off)
	 *
	 * @return			Smoothed data
	 */
	static VectorXd smooth_simple(const VectorXd& src, int len, int off);
};

#endif  // MOVING_AVERAGE_H
